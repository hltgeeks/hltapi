﻿using LearnerApp.Controllers;
using LearnerApp.Models;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http.Results;

namespace LearnerApp.Tests
{
    [TestFixture]
    class ReviewsControllerShould
    {
        TestInitializationSetup test;
        ReviewsController rc;




        [SetUp]
        public void Init()
        {
            rc = new ReviewsController();
            
             test = new TestInitializationSetup();
        }


        [Test]
        public void GetAllReviewIfPresent()
        {
            test.DeleteAll();
            test.AddData();


            //Arrange
            var actionResult = rc.Get();


            //Act
            var response = actionResult as NegotiatedContentResult<List<Review>>;
            // Assert.IsNotNull(response);
            var Reviews = response.Content;


            //Assert
            Assert.AreEqual(3, Reviews.Count());
        }


        [Test]
        public void GetAParticularReviewWhenValidIdIsGiven()
        {
            //Arrange
            var actionresult = rc.Get(1);
            //Act
            var response = actionresult as NegotiatedContentResult<Review>;
            Assert.IsNotNull(response);
            var Review = response.Content;
            //Assert
            Assert.AreEqual(1, Review.ID);
        }

        [Test]
        public void AddDataWhenValidReviewIsGiven()
        {
            var actionResult = rc.Post(new Review
            {
                
                Ratings = 2.4,
                Summary = "asdkjhwd",
                Title = "asdhkasj",
                CourseID = 1
            });
            var response = actionResult.ToString();
            //  Assert.IsNotNull(response);

            Assert.AreEqual("System.Web.Http.Results.OkResult", response);
        }

        [Test]
        public void DeleteDataWhenValidIdIsGiven()
        {
            //Arrange
            test.DeleteAll();
            test.AddData();
            var actionresult = rc.Delete(1);
            //Act
            var response = actionresult as OkNegotiatedContentResult<string>;
            string result = response.Content;
            //Assert
            Assert.AreEqual("Review Deleted Successfully", result);
        }
        
        [Test]
        public void UpdateDataWhenValidReviewIsGiven()
        {

            rc.Put(1, new Review() { ID = 1, Ratings = 5, Summary = "Updated", Title = "updated" });



        }

        [Test]
        public void ThrowAMessageWhenNoReviewsAreFound()
        {
            test.DeleteAll();
            //Arrange
            var actionresult = rc.Get();
            //Act
            var response = actionresult as NotFoundResult;
            //Assert
            string result = response.ToString();
            Assert.AreEqual("System.Web.Http.Results.NotFoundResult", result);
        }

        [Test]
        public void ThrowAMessageWhenTriedToGetReviewWithInvalidId()
        {
            test.DeleteAll();
            test.AddData();
            //Arrange
            var actionresult = rc.Get(400);
            //Act
            var response = actionresult as NegotiatedContentResult<string>;
            var result = response.Content.ToString();
            //Assert
            Assert.AreEqual("The Review you are searching for does not exist", result);

        }
        [Test]
        public void ThrowAMessageWhenTriedToUpdateAnInvalidReview()
        {
            //Arrange
            test.DeleteAll();
            test.AddData();
            var actionresult = rc.Put(300, new Review {  });

            //Act
            var response = actionresult as NotFoundResult;
            string result = response.ToString();
            //Assert
            Assert.AreEqual("System.Web.Http.Results.NotFoundResult", result);
        }

        [Test]
        public void ThrowAnErrorWhileDeletingAReviewWithInvalidId_NotFoundResult()
        {

            //Arrange
            var actionresult = rc.Delete(-32);

            //Act
            var response = actionresult as OkNegotiatedContentResult<string>;
            var result = response.Content.ToString();
            //Assert
            Assert.AreEqual("Review not Found", result);
        }
     
    }
}
