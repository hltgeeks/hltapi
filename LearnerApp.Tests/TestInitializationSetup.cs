﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LearnerApp.Repositories;
using LearnerApp.Models;
using LearnerApp.Controllers;

namespace LearnerApp.Tests
{
    

    public class TestInitializationSetup
    {
        SQLUnitOfWork UnitofWork { get; set; }
       
        LearnersAppDB SQLDbContext;

        public TestInitializationSetup()
        {
          
            UnitofWork = new SQLUnitOfWork();
           
        }

        public void DeleteAll()
        {
             UnitofWork.DeleteAllAndResetIdentity();
           
           


        }
        public void AddData()
        {
            SQLDbContext = new LearnersAppDB();

            UnitofWork.Course.Add(new Course { DurationInHrs = 45, Name = "Java", Overview = "sdw", Price = 2000.0, Reviews = null, Syllabus = null, Trainers = null });
            UnitofWork.Course.Add(new Course { DurationInHrs = 50, Name = "C#", Overview = "ssdfsadvfdw", Price = 20500.0, Reviews = null, Syllabus = null, Trainers = null });
            UnitofWork.Course.Add(new Course { DurationInHrs = 50, Name = "Web", Overview = "WebSite", Price = 18500.0, Reviews = null, Syllabus = null, Trainers = null });
            UnitofWork.commit();
            var aCourse = UnitofWork.Course.FindByID(1);
            var aCourse2 = UnitofWork.Course.FindByID(2);
            UnitofWork.Demo.Add(new Demo { Course = aCourse, CreateDate = DateTime.Now, CreatedBy = "Shwe", FromTime = DateTime.Now, ToTime = DateTime.Now });
            UnitofWork.Demo.Add(new Demo { Course = aCourse2, CreateDate = DateTime.Now, CreatedBy = "Shwetha", FromTime = DateTime.Now, ToTime = DateTime.Now });
            UnitofWork.commit();

            UnitofWork.Review.Add(new Review { ID = 1, Ratings = 4.2, Summary = "Good Learning Center", Title = "Course", CourseID = UnitofWork.Course.Get(course => course.Name == "Java").FirstOrDefault().ID });
            UnitofWork.Review.Add(new Review { ID = 22, Ratings = 2.2, Summary = "fwgsdfgdagftasd", Title = "Sambar", CourseID = UnitofWork.Course.Get(course => course.Name == "Web").FirstOrDefault().ID });
            UnitofWork.Review.Add(new Review { ID = 2, Ratings = 2.2, Summary = "fwgsdfdgadfsggsd", Title = "Idli", CourseID = UnitofWork.Course.Get(course => course.Name == "C#").FirstOrDefault().ID });
            UnitofWork.commit();

            var aReview = UnitofWork.Review.FindByID(1);
            var aReview2 = UnitofWork.Review.FindByID(2);

            var aDemo = UnitofWork.Demo.FindByID(1);
            var aDemo2 = UnitofWork.Demo.FindByID(2);
            UnitofWork.LearningCenter.Add(new LearningCenter() { Address = null, Courses = new List<Course>() { aCourse }, Reviews = new List<Review>() { aReview }, CreateDate = DateTime.Now, CreatedBy = "Shwetha", Name = "HLT", Demos = new List<Demo>() { aDemo } });
            UnitofWork.LearningCenter.Add(new LearningCenter() { Address = null, Courses = new List<Course>() { aCourse2}, Reviews = new List<Review>() { aReview2 }, Name = "Learning center 2", CreateDate = DateTime.Now, CreatedBy = "Shwetha", Demos = new List<Demo>() { aDemo2 } });
            UnitofWork.commit();

            var aLearningCenter = UnitofWork.LearningCenter.FindByID(1);
            var aLearningCenter2 = UnitofWork.LearningCenter.FindByID(2);
            UnitofWork.Category.Add(new Category { Name = "HLT", Description = "abcd", CreatedBy = "Shwe",LearningCenters = new List<LearningCenter>() { aLearningCenter} });
            UnitofWork.Category.Add(new Category { Name = "Driving", Description = "efgh", CreatedBy = "Shwetha",LearningCenters = new List<LearningCenter>() { aLearningCenter2} });
            UnitofWork.commit();

            
            UnitofWork.Demo.Add(new Demo { Course = aCourse, CreateDate = DateTime.Now, CreatedBy = "Shwe", FromTime = DateTime.Now, ToTime = DateTime.Now });
            UnitofWork.Demo.Add(new Demo { Course = aCourse2, CreateDate = DateTime.Now, CreatedBy = "Shwetha", FromTime = DateTime.Now, ToTime = DateTime.Now });
            UnitofWork.commit();
            //EC.Post(new Enquiry{ });
            //EC.Post(new Enquiry { });


            UnitofWork.Faculty.Add(new Faculty{ Name ="Amina",ID =12,DOB = new DateTime(1977,5,5), DOJ = new DateTime(1993,4,4),Experience = 2.0,HandledCourses =null });
           
            UnitofWork.Faculty.Add(new Faculty { Name = "Gita-Krishna Veni", ID = 12, DOB = new DateTime(2000, 5, 5), DOJ = new DateTime(2016, 4, 4), Experience = 12.0, HandledCourses = null });
            UnitofWork.commit();
           

           

           UnitofWork.Student.Add(new Student { Admissions = null, CollegeName = "aDE", contactNum = "5634", EducationLevel = "bca", Enquiries = null, StudentAddress = "fasdfsgf", StudentAge = 18, StudentDOB = new DateTime(2012, 5, 5), StudentId = 32 ,StudentName = "Adu-Byra-Anand"});
           UnitofWork.Student.Add(new Student { Admissions = null, CollegeName = "dhgfadj", contactNum = "563467565", EducationLevel = "BE", Enquiries = null, StudentAddress = "fasdfsgf", StudentAge = 18, StudentDOB = new DateTime(2012, 10, 5), StudentId = 22 ,StudentName = "Mamtha"});

            UnitofWork.commit();


            Course c = UnitofWork.Course.FindByID(1);
            Student s = UnitofWork.Student.FindByID(1);

            var e = new Enquiry(1, DateTime.Now, new List<Course>() { c }, "55", "NA", "NA", "NA", "NA", "NA", DateTime.Now, "NA", "NA");

            e.EnquiredStudent = s;
            e.ID = s.StudentId;

            UnitofWork.Enquiry.Add(e);

            UnitofWork.commit();
           

        }
    }
}
