﻿using LearnerApp.Core.Controllers;
using LearnerApp.Models;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http.Results;

namespace LearnerApp.Tests
{
    [TestFixture]
    class LearningCenterControllerShould
    {

        public LearningCentersController LCController { get; set; }
        public TestInitializationSetup test;
        


        [SetUp]
        public void Init()
        {
            LCController = new LearningCentersController();
            test = new TestInitializationSetup();
            test.DeleteAll();
            test.AddData();
        }
        [Test]

        public void GetAllLCPresentInDB()
        {

            var actionResult = LCController.Get();
            var response = actionResult.Result as NegotiatedContentResult<List<LearningCenter>>;
            var LCs = response.Content;

           
            
            //Assert
            Assert.AreEqual(2, LCs.Count());
        }

        [Test]
        public void GetParticularLearningCenterWhenValidIDIsGiven()
        {
            var actualResult = LCController.Get(1);
            var response = actualResult.Result as NegotiatedContentResult<LearningCenter>;
           
            Assert.IsNotNull(response);

            var LC = response.Content;
            Assert.AreEqual(LC.Name, "HLT");
            Assert.AreEqual(LC.CreatedBy, "Shwetha");
        }

        [Test]
        public void GetThrowsNotFoundErrorWhenLearningCenterAreNotPresentInDatabase()
        {
            test.DeleteAll();
            var actualResult =  LCController.Get(1);

           
            var response = actualResult.Result as NegotiatedContentResult<String>;

            Assert.AreEqual(response.Content ,"The Learning Center you are searching for does not exist");
        }

        [Test]
        public void ThrowAMessageWhenTriedToGetLearningCenterWithInvalidId()
        {

            var actionresult = LCController.Get(400);
            var response = actionresult.Result as NegotiatedContentResult<string>;

            Assert.AreEqual(response.Content, "The Learning Center you are searching for does not exist");
        }

        [Test]
        public void AddDataWhenValidLearningCenterIsGiven()
        {
            var actualResult = LCController.Post(new LearningCenter
            { Address = null, Courses = null, CreateDate = DateTime.Now, CreatedBy = "Shwe", Name = "HLT" });

            var response = actualResult.Result as NegotiatedContentResult<String>;

            Assert.AreEqual(response.Content, "Learning Center added successfully");
        }

        [Test]
        public void DeleteDataWhenValidLearningCenterIdIsGiven()
        {
      
            var actualResult = LCController.Delete(1);

            var response = actualResult.Result as NegotiatedContentResult<String>;


            Assert.AreEqual(response.Content, "Learning Center deleted Successfully");
        }

        [Test]
        public void ThrowAMessageWhenTriedToUpdateANonExixtingLearningCenter()
        {
           

            var actualResult = LCController.Put(-300,new LearningCenter
            { Address = null, Courses = null, CreateDate = DateTime.Now, CreatedBy = "Shwe", Name = "HLT" });

            var response = actualResult.Result as NegotiatedContentResult<String>;

            
            //Assert
            Assert.AreEqual(response.Content, "The Learning Center you are searching for does not exist");
        }

        [Test]
        public void ThrowAMessageWhenTriedToDeleteALearningCenterWithAnInvalidID()
        {
            var actualResult = LCController.Delete(-32);

            var response = actualResult.Result as NegotiatedContentResult<String>;

            Assert.AreEqual(response.Content, "The Learning Center you are searching for does not exist");
        }

       
    }
}
