﻿using LearnerApp.Controllers;
using LearnerApp.Models;
using LearnerApp.Repositories;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http.Results;

namespace LearnerApp.Tests
{
    [TestFixture]
    class LearningCenterCourseControllerShould
    {
        public LearningCenterCoursesController LCCController { get; set; }
        public SQLUnitOfWork UnitOfWork { get; set; }
        public TestInitializationSetup test;

        [SetUp]
        public void Init()
        {
            LCCController = new LearningCenterCoursesController();
            test = new TestInitializationSetup();
            test.DeleteAll();
            test.AddData();
        }

        [Test]
        public void GetParticularLearningCenterCourseWhenValidLearningCenterIDAndCourseIsGiven()
        {

            var actualResult = LCCController.Get(1,1);
            var response = actualResult.Result as NegotiatedContentResult<Course>;

            Assert.IsNotNull(response);

            var LCC = response.Content;
            Assert.AreEqual(1, LCC.ID);
        }

        [Test]
        public void GetThrowsNotFoundErrorWhenLearningCenterAreNotPresentInDatabase()
        {
            
            var actualResult = LCCController.Get(3, 5);
            var response = actualResult.Result as NegotiatedContentResult<String>;

            Assert.AreEqual(response.Content, "The Learning Center you are searching for does not exist");
        }

        [Test]
        public void GetThrowsNotFoundErrorWhenCoursesAreNotPresentLearningCenter()
        {
           
            var actualResult = LCCController.Get(1, 7);
            var response = actualResult.Result as NegotiatedContentResult<String>;

            Assert.AreEqual(response.Content, "The Course for a particular Learning Center does not exist");
        }

        [Test]
        public void ThrowAMessageWhenTriedToGetLearningCenterAndCourseWithInvalidId()
        {
            
            var actionresult = LCCController.Get(400,400);          
            var response = actionresult.Result as NegotiatedContentResult<String>;
            
            Assert.AreEqual(response.Content, "The Learning Center you are searching for does not exist");
        }

        [Test]
        public void DeleteDataWhenValidLearningCenterIdIsGiven()
        {
           

            var actualResult = LCCController.Delete(1);

            var response = actualResult.Result as NegotiatedContentResult<String>;


            Assert.AreEqual(response.Content, "Courses deleted Successfully");
        }

        [Test]
        public void DeleteDataWhenValidLearningCenterIdAndCourseIdIsGiven()
        {


            var actualResult = LCCController.Delete(1,1);

            var response = actualResult.Result as NegotiatedContentResult<String>;


            Assert.AreEqual(response.Content, "Courses deleted Successfully");
        }

        [Test]
        public void AddDataWhenValidCourseIsGiven()
        {

            var actionResult = LCCController.Post(1, new Course
            {
                ID = 1,
                DurationInHrs = 45,
                Name = "Web Designing",
                Overview = "asfsdas",
                Price = 20000,
                Reviews = null,
                Syllabus = null,
                Trainers = null

            });

            var response = actionResult.Result as NegotiatedContentResult<String>;
            
            Assert.AreEqual(response.Content, "Courses Added Successfully");
        }

    } 
}
