﻿using LearnerApp.Core.Controllers;
using LearnerApp.Models;
using LearnerApp.Repositories;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http.Results;

namespace LearnerApp.Tests
{
    class CategoryLearningCenterControllerShould
    {
        public CategoryLearningCentersController CLCController { get; set; }
        public SQLUnitOfWork UnitOfWork { get; set; }
        public TestInitializationSetup test;

        [SetUp]
        public void Init()
        {
            CLCController = new CategoryLearningCentersController();
            test = new TestInitializationSetup();
            test.DeleteAll();
            test.AddData();
        }

        [Test]
        public void GetParticularLearningCenterWhenValidCategoryIDAndLearningCenterIDAndReviewsGiven()
        {

            var actualResult = CLCController.Get(1, 1);
            var response = actualResult.Result as NegotiatedContentResult<Review>;

            Assert.IsNotNull(response);

            var LCR = response.Content;
            Assert.AreEqual(1, LCR.ID);
        }

        [Test]
        public void GetThrowsNotFoundErrorWhenCategoriesAreNotPresentInDatabase()
        {

            var actualResult = CLCController.Get(3, 5);
            var response = actualResult.Result as NegotiatedContentResult<String>;

            Assert.AreEqual(response.Content, "The Category you are searching for does not exist");
        }

        [Test]
        public void GetThrowsNotFoundErrorWhenLearningCenterAreNotPresentInCategories()
        {

            var actualResult = CLCController.Get(1, 5);
            var response = actualResult.Result as NegotiatedContentResult<String>;

            Assert.AreEqual(response.Content, "The Learning Center for a particular Category does not exist");
        }

        [Test]
        public void ThrowAMessageWhenTriedToGetCategoriesAndLearningCenterWithInvalidId()
        {

            var actionresult = CLCController.Get(400, 400);
            var response = actionresult.Result as NegotiatedContentResult<String>;

            Assert.AreEqual(response.Content, "The Category you are searching for does not exist");
        }

        [Test]
        public void DeleteDataWhenValidCategoryIdIsGiven()
        {
            
            var actualResult = CLCController.Delete(1);

            var response = actualResult.Result as NegotiatedContentResult<String>;


            Assert.AreEqual(response.Content, "Learning Center deleted Successfully");
        }

        [Test]
        public void DeleteDataWhenValidCategoryIdAndLearningCenterIdIsGiven()
        {

            var actualResult = CLCController.Delete(1,1);

            var response = actualResult.Result as NegotiatedContentResult<String>;


            Assert.AreEqual(response.Content, "Learning Center deleted Successfully");
        }

        [Test]
        public void AddDataWhenValidLearningCenterIsGiven()
        {

            var actionResult = CLCController.Post(1, new LearningCenter
            {
                Name = "New Learning Center",
                CreatedBy = "Tom"
               
                
            });

            var response = actionResult.Result as NegotiatedContentResult<String>;

            Assert.AreEqual(response.Content, "Learning Center added Successfully");
        }
    }
}
