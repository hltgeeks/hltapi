﻿using LearnerApp.Core.Controllers;
using LearnerApp.Models;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http.Results;

namespace LearnerApp.Tests
{
    [TestFixture]
    class CategoryControllerShould
    {
        public CategoriesController CategoryController { get; set; }
        public TestInitializationSetup test;

        [SetUp]
        public void Init()
        {
            CategoryController = new CategoriesController();
            test = new TestInitializationSetup();
            test.DeleteAll();
            test.AddData();
        }

        [Test]

        public void GetAllCategoryPresentInDB()
        {
            var actionResult = CategoryController.Get();
            var response = actionResult.Result as NegotiatedContentResult<List<Category>>;
            var CC = response.Content;

            //Assert
            Assert.AreEqual(2, CC.Count());
        }

        [Test]
        public void GetParticularCategoryWhenValidIDIsGiven()
        {
            var actualResult = CategoryController.Get(1);
            var response = actualResult.Result as NegotiatedContentResult<Category>;

            Assert.IsNotNull(response);

            var CC = response.Content;
            Assert.AreEqual(CC.Name, "HLT");
            Assert.AreEqual(CC.CreatedBy, "Shwe");
        }

        [Test]
        public void GetThrowsNotFoundErrorWhenCategoryAreNotPresentInDatabase()
        {
            test.DeleteAll();

            var actualResult = CategoryController.Get(1);
            var response = actualResult.Result as NegotiatedContentResult<String>;

            Assert.AreEqual(response.Content, "Categories you are searching for does not exist");
        }

        [Test]
        public void ThrowAMessageWhenTriedToGetCategoryWithInvalidId()
        {

            var actionresult = CategoryController.Get(400);
            var response = actionresult.Result as NegotiatedContentResult<string>;

            Assert.AreEqual(response.Content, "Categories you are searching for does not exist");
        }

        [Test]
        public void AddDataWhenValidCategoriesIsGiven()
        {
            var actualResult = CategoryController.Post(new Category
            { Name = "HLT", Description = "abcd", CreatedBy = "Shwe" });

            var response = actualResult.Result as NegotiatedContentResult<String>;

            Assert.AreEqual(response.Content, "Category added successfully");
        }

        [Test]
        public void DeleteDataWhenValidCategoryIdIsGiven()
        {

            var actualResult = CategoryController.Delete(1);

            var response = actualResult.Result as NegotiatedContentResult<String>;


            Assert.AreEqual(response.Content, "Category deleted Successfully");
        }

        [Test]
        public void ThrowAMessageWhenTriedToUpdateANonExixtingCategory()
        {

            var actualResult = CategoryController.Put(-300, new Category
            { Name = "HLT", Description = "abcd", CreatedBy = "Shwe", CreateDate=DateTime.Now });

            var response = actualResult.Result as NegotiatedContentResult<String>;


            //Assert
            Assert.AreEqual(response.Content, "The Category you are searching for does not exist");
        }

        [Test]
        public void ThrowAMessageWhenTriedToDeleteACategoryWithAnInvalidID()
        {
            var actualResult = CategoryController.Delete(-32);

            var response = actualResult.Result as NegotiatedContentResult<String>;

            Assert.AreEqual(response.Content, "Category you are searching for does not exist");
        }

    }
}
