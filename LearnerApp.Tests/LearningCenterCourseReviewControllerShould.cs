﻿using LearnerApp.Core.Controllers;
using LearnerApp.Models;
using LearnerApp.Repositories;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http.Results;

namespace LearnerApp.Tests
{
    [TestFixture]
    class LearningCenterCourseReviewControllerShould
    {
        public LearningCenterCourseReviewController LCCRController { get; set; }
        public SQLUnitOfWork UnitOfWork { get; set; }
        public TestInitializationSetup test;

        [SetUp]
        public void Init()
        {
            LCCRController = new LearningCenterCourseReviewController();
            test = new TestInitializationSetup();
            test.DeleteAll();
            test.AddData();
        }

        [Test]
        public void GetParticularLearningCenterCourseReviewWhenValidLearningCenterIdCourseIdAndReviewIdIsGiven()
        {

            var actualResult = LCCRController.Get(1, 1, 1);
            var response = actualResult.Result as NegotiatedContentResult<Review>;

            Assert.IsNotNull(response);

            var LCCR = response.Content;
            Assert.AreEqual(1, LCCR.ID);
        }

        [Test]
        public void GetThrowsNotFoundErrorWhenLearningCenterAndCoursesAreNotPresentInDatabase()
        {

            var actualResult = LCCRController.Get(4, 1, 1);
            var response = actualResult.Result as NegotiatedContentResult<String>;

            Assert.AreEqual(response.Content, "The Learning Center you are searching for does not exist");
        }

        [Test]
        public void GetThrowsNotFoundErrorWhenCoursesAreNotPresentInLearningCenter()
        {

            var actualResult = LCCRController.Get(1, 5, 1);
            var response = actualResult.Result as NegotiatedContentResult<String>;

            Assert.AreEqual(response.Content, "The Course for a particular Learning Center does not exist");
        }

        [Test]
        public void GetThrowsNotFoundErrorWhenReviewsAreNotPresentInCourse()
        {

            var actualResult = LCCRController.Get(1, 1, 5);
            var response = actualResult.Result as NegotiatedContentResult<String>;

            Assert.AreEqual(response.Content, "The Review for a particular Course does not exist");
        }

        [Test]
        public void ThrowAMessageWhenTriedToGetLearningCenterCourseAndReviewsWithInvalidId()
        {

            var actionresult = LCCRController.Get(400, 400, 400);
            var response = actionresult.Result as NegotiatedContentResult<String>;

            Assert.AreEqual(response.Content, "The Learning Center you are searching for does not exist");
        }

        [Test]
        public void DeleteDataWhenValidLearningCenterIdAndCourseIdIsGiven()
        {


            var actualResult = LCCRController.Delete(1,1);

            var response = actualResult.Result as NegotiatedContentResult<String>;


            Assert.AreEqual(response.Content, "Reviews deleted Successfully");
        }

        [Test]
        public void DeleteDataWhenValidLearningCenterIdCourseIdAndReviewIdIsGiven()
        {
            var actualResult = LCCRController.Delete(1, 1, 1);

            var response = actualResult.Result as NegotiatedContentResult<String>;


            Assert.AreEqual(response.Content, "Reviews deleted Successfully");
        }

        [Test]
        public void AddDataWhenValidCourseIsGiven()
        {

            var actionResult = LCCRController.Post(1,1, new Review
            {
              Title = "New Title",
              Summary = "New Summary",
              Ratings = 4.2             
            });

            var response = actionResult.Result as NegotiatedContentResult<String>;

            Assert.AreEqual(response.Content, "Reviews added Successfully");
        }
    }
}
