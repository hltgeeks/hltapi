﻿using LearnerApp.Controllers;
using LearnerApp.Models;
using LearnerApp.Repositories;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Results;

namespace LearnerApp.Tests
{
    [TestFixture]
  public  class EnquiryControllerShould
    {
        TestInitializationSetup test;
        EnquiryController ec;
        SQLUnitOfWork uof;
       [SetUp]
        public void Init()
        {

            uof = new SQLUnitOfWork();
             ec = new EnquiryController(uof);
            test = new TestInitializationSetup();
        }




        [Test]
        public void GetAllEnquiriesIfPresent()
        {
            test.DeleteAll();
            test.AddData();
            //Arrange
            var actionResult = ec.Get();
            
            //Act
            var response = actionResult as NegotiatedContentResult<List<Enquiry>>;
            // Assert.IsNotNull(response);
            var Enquiries = response.Content;


            //Assert
            Assert.IsTrue(Enquiries.Count()>0);
        }


        [Test]
        public void GetAParticularEnquiryWhenValidIdIsGiven()
        {

            //test.DeleteAll();
            //test.AddData();
            
            ////Arrange
            //var actionresult = ec.Get(7);
            ////Act

            ////IHttpActionResult response = actionresult;// as NegotiatedContentResult<Enquiry>;
            ////Assert.IsNotNull(response);
            ////var enquiry = response.Content;

            ////Assert
            //Assert.AreEqual(7,enquiry.ID);
        }

        [Test]
        public void AddEnquiryWhenValidEnquiryIsGiven()
        {

            //Arrange
            test.DeleteAll();
            test.AddData();

            Course c = uof.Course.FindByID(1);
            Student s = uof.Student.FindByID(1);

            var e = new Enquiry(1, DateTime.Now, new List<Course>() { c }, "55", "NA", "NA", "NA", "NA", "NA", DateTime.Now, "NA", "NA");

            e.EnquiredStudent = s;
            e.ID = s.StudentId;
            var actionresult = ec.Post(e);

            //Act
            var response = actionresult as OkResult;
            var result = actionresult.ToString();
            //Assert       
        Assert.AreEqual("System.Web.Http.Results.OkResult", result);

        }



        [Test]
        public void DeleteDataWhenValidIdIsGiven()
        {
            test.DeleteAll();
            test.AddData();
            //Arrange
            var actionresult = ec.Delete(1);
            //Act
            var response = actionresult as OkNegotiatedContentResult<string>;
            //Assert
            var result = response.Content;
            Assert.AreEqual("Enquiry Deleted Successfully", result);
        }



        [Test]
        public void UpdateDataWhenValidCourseIsGiven()
        {

        }

        [Test]
        public void ThrowAMessageWhenNoEnquiriesAreFound()
        {
            test.DeleteAll();
            //Arrange
            var actionresult = ec.Get();
            //Act
            var response = actionresult as NotFoundResult;
            //Assert
            string result = response.ToString();
            Assert.AreEqual("System.Web.Http.Results.NotFoundResult", result);
        }
        [Test]
        public void ThrowAMessageWhenTriedToGetEnquiryWithInvalidId()
        {
            test.DeleteAll();
            test.AddData();
            //Arrange
            var actionresult = ec.Get(400);
            //Act
            var response = actionresult as NegotiatedContentResult<string>;
            var result = response.Content.ToString();
            //Assert
            Assert.AreEqual("The Enquiry you are searching for does not exist", result);
        }

        [Test]
        public void ThrowAMessageWhenTriedToUpdateAnInvalidEnquiry()
        {
            //Arrange
            test.DeleteAll();
            test.AddData();
            var actionresult = ec.Put(300, new Enquiry { ContactNum = "5646546",Courses =null,Date =new DateTime(1992,2,1),Duration ="2",EmailId = "asdsdfs", EnquiredStudent = null });

            //Act
            var response = actionresult as NotFoundResult;
            string result = response.ToString();
            //Assert
            Assert.AreEqual("System.Web.Http.Results.NotFoundResult", result);
        }
        [Test]
        public void ThrowAnErrorWhileDeletingAEnquiryWithInvalidId_NotFoundResult()
        {

            //Arrange
            var actionresult = ec.Delete(-32);

            //Act
            var response = actionresult as OkNegotiatedContentResult<string>;
            var result = response.Content.ToString();
            //Assert
            Assert.AreEqual("Enquiry not Found", result);
        }
        
 
        [Ignore("The Test Case is Out of Scope")]
        [Test]
        public void ThrowAnErrorWhenTriedToUpdateWithInavlidEntries()
        {
            //Arrange
            ec.ModelState.AddModelError("FirstName", "First Name is Required");
            var actionresult = ec.Put(14, new Enquiry { });
            //Act
            var response = actionresult as BadRequestErrorMessageResult;
            var result = response.Message.ToString();
            //Assert
            Assert.AreEqual("There are errors in the Form Entry", result);
        }
    }
}
