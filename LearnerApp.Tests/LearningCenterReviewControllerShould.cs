﻿using LearnerApp.Controllers;
using LearnerApp.Models;
using LearnerApp.Repositories;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http.Results;

namespace LearnerApp.Tests
{
    class LearningCenterReviewControllerShould
    {
        public LearningCenterReviewsController LCRController { get; set; }
        public SQLUnitOfWork UnitOfWork { get; set; }
        public TestInitializationSetup test;

        [SetUp]
        public void Init()
        {
            LCRController = new LearningCenterReviewsController();
            test = new TestInitializationSetup();
            test.DeleteAll();
            test.AddData();
        }

        [Test]
        public void GetParticularLearningCenterReviewWhenValidLearningCenterIDAndReviewIsGiven()
        {

            var actualResult = LCRController.Get(1, 1);
            var response = actualResult.Result as NegotiatedContentResult<Review>;

            Assert.IsNotNull(response);

            var LCR = response.Content;
            Assert.AreEqual(1, LCR.ID);
        }

        [Test]
        public void GetThrowsNotFoundErrorWhenLearningCenterAreNotPresentInDatabase()
        {

            var actualResult = LCRController.Get(3, 5);
            var response = actualResult.Result as NegotiatedContentResult<String>;

            Assert.AreEqual(response.Content, "The Learning Center you are searching for does not exist");
        }

        [Test]
        public void GetThrowsNotFoundErrorWhenReviewsAreNotPresentInLearningCenter()
        {

            var actualResult = LCRController.Get(1, 5);
            var response = actualResult.Result as NegotiatedContentResult<String>;

            Assert.AreEqual(response.Content, "The Review for a particular Learning Center does not exist");
        }

        [Test]
        public void ThrowAMessageWhenTriedToGetLearningCenterAndReviewWithInvalidId()
        {

            var actionresult = LCRController.Get(400, 400);
            var response = actionresult.Result as NegotiatedContentResult<String>;

            Assert.AreEqual(response.Content, "The Learning Center you are searching for does not exist");
        }

        [Test]
        public void DeleteDataWhenValidLearningCenterIdIsGiven()
        {


            var actualResult = LCRController.Delete(1);

            var response = actualResult.Result as NegotiatedContentResult<String>;


            Assert.AreEqual(response.Content, "Reviews deleted Successfully");
        }

        [Test]
        public void DeleteDataWhenValidLearningCenterIdAndReviewIdIsGiven()
        {


            var actualResult = LCRController.Delete(1,1);

            var response = actualResult.Result as NegotiatedContentResult<String>;


            Assert.AreEqual(response.Content, "Reviews deleted Successfully");
        }

        [Test]
        public void AddDataWhenValidReviewIsGiven()
        {

            var actionResult = LCRController.Post(1, new Review
            { ID = 1,
              Ratings = 4.2,
              Summary = "Best Learning Center",
              Title = "Review",
              CourseID = 1
            });

            var response = actionResult.Result as NegotiatedContentResult<String>;

            Assert.AreEqual(response.Content, "Reviews added Successfully");
        }

    }
}
