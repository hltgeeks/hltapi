﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LearnerApp.Controllers;
using LearnerApp.Models;
using System.Web.Http;
using System.Web.Http.Results;
using System.Net.Http;
using System.Net;

namespace LearnerApp.Tests
{
    [TestFixture]
  public  class StudentControllerShould
    {
        TestInitializationSetup test;
        StudentsController sc;

        [SetUp]
        public void Init()
        {
            sc = new StudentsController();
            test = new TestInitializationSetup();
        }

        [Test]
        public void GetAllStudentsIfPresent()
        {
            test.DeleteAll();
            test.AddData();

            //Arrange
            var actionResult = sc.Get();


            //Act
            var response = actionResult as NegotiatedContentResult<List<Student>>;
           // Assert.IsNotNull(response);
            var Students = response.Content;


            //Assert
            Assert.AreEqual(2, Students.Count());
        }


        [Test]
        public void GetAParticularStudentWhenValidIdIsGiven()
        {
            test.DeleteAll();
            test.AddData();
            //Arrange
            var actionresult = sc.Get(1);
            //Act
            var response = actionresult as NegotiatedContentResult<Student>;
            Assert.IsNotNull(response);
            var Student = response.Content;
            //Assert
            Assert.AreEqual(1, Student.StudentId);
        }

        [Test]
        public void AddDataWhenValidStudentIsGiven()
        {
            var actionResult = sc.Post(new Student
            {
                Admissions = null,
                CollegeName = "Manuuu",
                contactNum= "2453865465465465",
                EducationLevel = "gdegadg",
                 Enquiries = null,
                 StudentAddress = "fwegyeqjuejutrhfg",
                StudentAge = 22,
                 StudentDOB = new DateTime(2014,2,2),
                 StudentId =63 ,
                  StudentName= "eGRADFGDFG" 

            });
            var response = actionResult as NegotiatedContentResult<string>;
            //  Assert.IsNotNull(response);
            var responsestring = response.Content;

            Assert.AreEqual("Student Added sucessfully", responsestring);
        }

        [Test]
        public void DeleteDataWhenValidIdIsGiven()
        {
            //Arrange
            test.DeleteAll();
            test.AddData();
            var actionresult = sc.Delete(2);
            //Act
            var response = actionresult as OkNegotiatedContentResult<string>;
            string result = response.Content;
            //Assert
            Assert.AreEqual("Student deleted Successfully", result);

        }



        [Test]
        public void UpdateDataWhenValidStudentIsGiven()
        {

        }



        [Test]

        public void ThrowAMessageWhenNoStudentsAreFound()
        {
            test.DeleteAll();
            //Arrange
            var actionresult = sc.Get();
            //Act
            var response = actionresult as NegotiatedContentResult<string>;
            //Assert
            string result = response.Content.ToString();
            Assert.AreEqual("No Students added Yet", result);
        }

        [Test]
        public void ThrowAMessageWhenTriedToGetStudentWithInvalidId()
        {
            test.DeleteAll();
            test.AddData();
            //Arrange
            var actionresult = sc.Get(400);
            //Act
            var response = actionresult as NegotiatedContentResult<string>;
            var result = response.Content.ToString();
            //Assert
            Assert.AreEqual("No Students added Yet", result);
        }
        [Test]
        public void ThrowAnErrorWhileDeletingAStudentWithInvalidId_NotFoundResult()
        {

            //Arrange
            var actionresult = sc.Delete(-32);

            //Act
            var response = actionresult as OkNegotiatedContentResult<string>;
            var result = response.Content.ToString();
            //Assert
            Assert.AreEqual("Student Not found", result);
        }
        [Test]
        public void ThrowAnErrorWhenTriedToAddAnInvalidStudent()
        {
            //Arrange
            sc.ModelState.AddModelError("First Name", "First Name is Required");
            var actionresult = sc.Post(new Student{ });

            //Act
            var response = actionresult as NegotiatedContentResult<string>;
            var result = response.Content.ToString();
            //Assert
            Assert.AreEqual("Error Adding Student", result);
        }

       
    }

}

