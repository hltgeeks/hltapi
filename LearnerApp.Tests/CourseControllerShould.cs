﻿using LearnerApp.Controllers;
using LearnerApp.Models;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http.Results;

namespace LearnerApp.Tests
{
    [TestFixture]
    public class CourseControllerShould
    {
        CoursesController cc;
        TestInitializationSetup test;


        [SetUp]
        public void Init()
        {
            cc = new CoursesController();
            test = new TestInitializationSetup();
        }


        [Test]
        public void GetAllCoursesIfPresent()
        {

            
            test.DeleteAll();
            test.AddData();

            //Arrange
            var actionResult = cc.Get();


            //Act
            var response = actionResult.Result as NegotiatedContentResult<List<Course>>;
            // Assert.IsNotNull(response);
            var Courses = response.Content;


            //Assert
            Assert.True(Courses.Count()>0);
        }


        [Test]
        public void GetAParticularCourseWhenValidIdIsGiven()
        {

            //Arrange
            test.DeleteAll();
            test.AddData();

            var actionresult = cc.Get(1);
            //Act
            var response = actionresult.Result as NegotiatedContentResult<Course>;
            Assert.IsNotNull(response);
            var course = response.Content;

            //Assert
            Assert.AreEqual(1,course.ID);

        }

        [Test]
        public async Task  AddDataWhenValidCourseIsGiven()
        {

            //CreateDate = DateTime.Now ,ModifiedDate = DateTime.Now,
            var actionResult =await cc.Post(new Course
            {ID= 1,DurationInHrs =45,Name = "agjf",Overview="asfsdas",Price = 20.00,Reviews = null,Syllabus = null,Trainers = null
                
            });
            
            var response = actionResult.ToString();
            //  Assert.IsNotNull(response);
            
            Assert.AreEqual("System.Web.Http.Results.OkResult", response);
        }

        [Test]
        public void DeleteDataWhenValidIdIsGiven()
        {
            //Arrange
            //test.DeleteAll();
            //test.AddData();

          
            var actionresult = cc.Delete(1);
            //Act
            var response = actionresult.Result as NegotiatedContentResult<String>;
            //Assert
            // var result = response.Content;
            Assert.AreEqual(response.Content, "Course deleted Successfully");
        }



        [Test]
        public void UpdateDataWhenValidCourseIsGiven()
        {

        }



        [Test]
        public void ThrowAnErrorWhileDeletingACourseWithInvalidId_NotFoundResult()
        {
            test.DeleteAll();
            //Arrange
            var actionresult = cc.Get();
            //Act
            var response =actionresult.Result as NotFoundResult;
            //Assert
            string result = response.ToString();
            Assert.AreEqual("System.Web.Http.Results.NotFoundResult",result);
        }

        [Test]
        public void ThrowAMessageWhenTriedToGetCourseWithInvalidId()
        {
            test.DeleteAll();
            test.AddData();
            //Arrange
            var actionresult = cc.Get(400);
            //Act
            var response = actionresult.Result as NegotiatedContentResult<string>;
            var result = response.Content.ToString();
            //Assert
            Assert.AreEqual("The course you are searching for does not exist", result);
        }

        //[Test]
        //public void ThrowAnErrorWhenTriedToAddAnInvalidCourse()
        //{
        //    //Arrange
        //    cc.ModelState.AddModelError("FirstName", "First Name is Required");
        //    var actionresult = cc.Post(new Course { });
        //    //Act
        //    var response = actionresult.Result as NegotiatedContentResult<string>;
        //    var result = response.Content.ToString();
        //    //Assert
        //    Assert.AreEqual("Error Adding Course", result);

        //}

        [Test]
        public void ThrowAMessageWhenTriedToUpdateAnInvalidCourse()
        {
            //Arrange
            test.DeleteAll();
            test.AddData();
            var actionresult = cc.Put(-300,new Course { DurationInHrs= 25, ID =22,Name ="jgsshi", Overview = "askahkd", Price =25,Reviews =null, Syllabus= null,Trainers = null});

            //Act
            var response = actionresult.Result as NotFoundResult;
            string result = response.ToString();
            //Assert
            Assert.AreEqual("System.Web.Http.Results.NotFoundResult", result);
        }

        [Test]
        public void ThrowAMessageWhenTriedToDeleteACourseWithAnInvalidID()
        {
            //Arrange
            var actionresult = cc.Delete(-32);

            //Act
            var response = actionresult.Result as NegotiatedContentResult<string>;
            var result = response.Content.ToString();
            //Assert
            Assert.AreEqual("Course Not found", result);
        }

    }
}
