﻿using LearnerApp.Core.Controllers;
using LearnerApp.Models;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http.Results;

namespace LearnerApp.Tests
{
    [TestFixture]
    class DemoControllerShould
    {
        public DemosController DController { get; set; }
        public TestInitializationSetup test;

        [SetUp]
        public void Init()
        {
            DController = new DemosController();
            test = new TestInitializationSetup();
            test.DeleteAll();
            test.AddData();
        }

        [Test]
        public void GetAllDemoPresentInDB()
        {

            var actionResult = DController.Get();
            var response = actionResult.Result as NegotiatedContentResult<List<Demo>>;
            var LCs = response.Content;

            Assert.AreEqual(2, LCs.Count());
        }

        [Test]
        public void GetParticularDemoWhenValidIDIsGiven()
        {
            var actualResult = DController.Get(1);
            var response = actualResult.Result as NegotiatedContentResult<Demo>;

            Assert.IsNotNull(response);

            var Demo = response.Content;           
            Assert.AreEqual(Demo.CreatedBy, "Shwe");
        }

        [Test]
        public void GetThrowsNotFoundErrorWhenDemosAreNotPresentInDatabase()
        {
            test.DeleteAll();
            var actualResult = DController.Get(1);


            var response = actualResult.Result as NegotiatedContentResult<String>;

            Assert.AreEqual(response.Content, "Demo you are searching for does not exist");
        }

        [Test]
        public void ThrowAMessageWhenTriedToGetDemoWithInvalidId()
        {

            var actionresult = DController.Get(400);
            var response = actionresult.Result as NegotiatedContentResult<string>;

            Assert.AreEqual(response.Content, "Demo you are searching for does not exist");
        }

        [Test]
        public void AddDataWhenValidDemoIsGiven()
        {
            var actualResult = DController.Post(new Demo
            { FromTime = DateTime.Now, ToTime = DateTime.Now, CreateDate = DateTime.Now, CreatedBy = "Shwe" });

            var response = actualResult.Result as NegotiatedContentResult<String>;

            Assert.AreEqual(response.Content, "Demo added successfully");
        }

        [Test]
        public void DeleteDataWhenValidDemoIdIsGiven()
        {
            var actualResult = DController.Delete(1);

            var response = actualResult.Result as NegotiatedContentResult<String>;


            Assert.AreEqual(response.Content, "Demo deleted Successfully");
        }

        [Test]
        public void ThrowAMessageWhenTriedToUpdateANonExixtingDemo()
        {
            var actualResult = DController.Put(-300, new Demo
            { FromTime = DateTime.Now, ToTime = DateTime.Now, CreateDate = DateTime.Now, CreatedBy = "Shwe" });

            var response = actualResult.Result as NegotiatedContentResult<String>;

            Assert.AreEqual(response.Content, "Demo you are searching for does not exist");
        }

        [Test]
        public void ThrowAMessageWhenTriedToDeleteADemoWithAnInvalidID()
        {
            var actualResult = DController.Delete(-32);

            var response = actualResult.Result as NegotiatedContentResult<String>;

            Assert.AreEqual(response.Content, "Demo you are searching for does not exist");
        }

    }
}
