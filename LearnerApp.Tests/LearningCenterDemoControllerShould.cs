﻿using LearnerApp.Core.Controllers;
using LearnerApp.Models;
using LearnerApp.Repositories;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http.Results;

namespace LearnerApp.Tests
{
    class LearningCenterDemoControllerShould
    {
        public LearningCenterDemosController LCDController { get; set; }
        public SQLUnitOfWork UnitOfWork { get; set; }
        public TestInitializationSetup test;

        [SetUp]
        public void Init()
        {
            LCDController = new LearningCenterDemosController();
            test = new TestInitializationSetup();
            test.DeleteAll();
            test.AddData();
        }

        [Test]
        public void GetParticularLearningCenterDemoWhenValidLearningCenterIDAndDemoIDIsGiven()
        {

            var actualResult = LCDController.Get(1, 1);
            var response = actualResult.Result as NegotiatedContentResult<Demo>;

            Assert.IsNotNull(response);

            var LCR = response.Content;
            Assert.AreEqual(1, LCR.ID);
        }

        [Test]
        public void GetThrowsNotFoundErrorWhenLearningCenterAreNotPresentInDatabase()
        {

            var actualResult = LCDController.Get(3, 5);
            var response = actualResult.Result as NegotiatedContentResult<String>;

            Assert.AreEqual(response.Content, "The Learning Center you are searching for does not exist");
        }

        [Test]
        public void GetThrowsNotFoundErrorWhenDemosAreNotPresentInLearningCenter()
        {

            var actualResult = LCDController.Get(1, 5);
            var response = actualResult.Result as NegotiatedContentResult<String>;

            Assert.AreEqual(response.Content, "Demo for a particular Learning Center does not exist");
        }

        [Test]
        public void ThrowAMessageWhenTriedToGetLearningCenterAndDemoWithInvalidId()
        {

            var actionresult = LCDController.Get(400, 400);
            var response = actionresult.Result as NegotiatedContentResult<String>;

            Assert.AreEqual(response.Content, "The Learning Center you are searching for does not exist");
        }

        [Test]
        public void DeleteDataWhenValidLearningCenterIdIsGiven()
        {
            
            var actualResult = LCDController.Delete(1);

            var response = actualResult.Result as NegotiatedContentResult<String>;


            Assert.AreEqual(response.Content, "Demos deleted Successfully");
        }

        [Test]
        public void DeleteDataWhenValidLearningCenterIdAndDemoIdIsGiven()
        {
            var actualResult = LCDController.Delete(1, 1);

            var response = actualResult.Result as NegotiatedContentResult<String>;


            Assert.AreEqual(response.Content, "Demo deleted Successfully");
        }

        [Test]
        public void AddDataWhenValidDemoIsGiven()
        {

            var actionResult = LCDController.Post(1, new Demo
            {
                ID = 1,
                FromTime = DateTime.Now,
                ToTime = DateTime.Now,
                CreateDate = DateTime.Now,
                CreatedBy = "Shwe"
                
            });

            var response = actionResult.Result as NegotiatedContentResult<String>;

            Assert.AreEqual(response.Content, "Demos added Successfully");
        }
    }
}
