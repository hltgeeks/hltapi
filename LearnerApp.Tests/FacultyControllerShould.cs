﻿using LearnerApp.Controllers;
using LearnerApp.Models;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http.Common;
using System.Web.Http.Results;

namespace LearnerApp.Tests
{
    [TestFixture]
    class FacultyControllerShould
    {
        TestInitializationSetup test;
        FacultyController fc;


        [SetUp]
        public void Init()
        {
            fc = new FacultyController();
            test = new TestInitializationSetup();
        }



        [Test]
        public void GetAllFacultyIfPresent()
        {

            test.DeleteAll();
            test.AddData();

            //Arrange
            var actionResult = fc.Get();


            //Act
            var response = actionResult as NegotiatedContentResult<List<Faculty>>;
            // Assert.IsNotNull(response);
            var Faculties = response.Content;


            //Assert
            Assert.AreEqual(2, Faculties.Count());
        }


        [Test]
        public void GetAParticularFacultyWhenValidIdIsGiven()
        {
            //Arrange
            var actionresult = fc.Get(1);
            //Act
            var response = actionresult as NegotiatedContentResult<Faculty>;
            Assert.IsNotNull(response);
            var Faculty = response.Content;
            //Assert
            Assert.AreEqual(1,Faculty.ID);
        }

        [Test]
        public void AddDataWhenValidCourseIsGiven()
        {
            //Arrange
            var actionResult = fc.Post(new Faculty
            {
              ID = 52,
              DOB = new DateTime(2015,2,2),
              DOJ = new DateTime(2015,6,8),
              Experience = 2.00,
              HandledCourses = null,
              Name ="shanmugam"
              

            });
            var response = actionResult.ToString();
            //  Assert.IsNotNull(response);

            Assert.AreEqual("System.Web.Http.Results.OkResult", response);
           
        }

        [Test]
        public void DeleteDataWhenValidIdIsGiven()
        {
            //Arrange
            test.DeleteAll();
            test.AddData();
            var actionresult = fc.Delete(2);

            //Act
            var response = actionresult as OkNegotiatedContentResult<string>;
            var result = response.Content;
            //Assert

            Assert.AreEqual("Faculty deleted successfully", result);
           


        }



        [Test]
        public void UpdateDataWhenValidFacultyIsGiven()
        {
            //Arrange
            //Act
            //Assert
        }


        [Test]
        public void ThrowAMessageWhenNoFacultiesAreFound()
        {

            test.DeleteAll();
            //Arrange
            var actionresult = fc.Get();
            //Act
            var response = actionresult as NotFoundResult;
            //Assert
            string result = response.ToString();
            Assert.AreEqual("System.Web.Http.Results.NotFoundResult", result);

        }

        [Test]
        public void ThrowAMessageWhenTriedToGetFacultyWithInvalidId()
        {
            //Arrange
            test.DeleteAll();
            test.AddData();
            var actionresult = fc.Get(5241);

            //Act
            var response = actionresult as NegotiatedContentResult<string>;
            var result = response.Content.ToString();
            //Assert
            Assert.AreEqual("The faculty You are searching for does not Exist",result);
        }
        [Test]
        public void ThrowAMessageWhenTriedToUpdateAnInvalidFaculty()
        {
            //Arrange
            test.DeleteAll();
            test.AddData();
            var actionresult = fc.Put(300, new Faculty {  });

            //Act
            var response = actionresult as NotFoundResult;
            string result = response.ToString();
            //Assert
            Assert.AreEqual("System.Web.Http.Results.NotFoundResult", result);
        }
        [Test]
        public void ThrowAnErrorWhileDeletingAFacultyWithInvalidId_NotFoundResult()
        {

            //Arrange
            var actionresult = fc.Delete(-32);

            //Act
            var response = actionresult as OkNegotiatedContentResult<string>;
            var result = response.Content.ToString();
            //Assert
            Assert.AreEqual("Faculty not found", result);
        }
    }
}
