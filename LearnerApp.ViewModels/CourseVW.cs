﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LearnerApp.ViewModels
{
    public class CourseVW
    {

        public int ID { get; set; }
        
        public String Name { get; set; }
        public string Overview { get; set; }

        public string Syllabus { get; set; }

        public string Ctype { get; set; }

        public double Price { get; set; }

        public IEnumerable<string> FacultyNames { get; set; }
        public  IEnumerable<ReviewVW> Reviews { get; set; }
        public double DurationInHrs { get; set; }













    }
}
