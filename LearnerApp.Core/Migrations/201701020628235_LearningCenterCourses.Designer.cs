// <auto-generated />
namespace LearnerApp.Core.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class LearningCenterCourses : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(LearningCenterCourses));
        
        string IMigrationMetadata.Id
        {
            get { return "201701020628235_LearningCenterCourses"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
