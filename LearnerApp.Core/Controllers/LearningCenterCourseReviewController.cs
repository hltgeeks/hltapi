﻿using LearnerApp.Models;
using LearnerApp.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace LearnerApp.Core.Controllers
{
    [RoutePrefix("api")]
    public class LearningCenterCourseReviewController : ApiController
    {
        public SQLUnitOfWork UnitOfWork { get; set; }
        public IRepository<LearningCenter> LCRepo { get; set; }
        public IRepository<Course> CourseRepo { get; set; }
        public IRepository<Review> ReviewRepo { get; set; }

        public LearningCenterCourseReviewController()
        {
            UnitOfWork = new SQLUnitOfWork();
            LCRepo = UnitOfWork.LearningCenter;
            CourseRepo = UnitOfWork.Course;
            ReviewRepo = UnitOfWork.Review;
        }

        // GET: api/LearningCenters/1/Course/1/Reviews
        [Route("LearningCenters/{LearningCentersId}/Course/{CourseId}/Reviews")]
        public async Task<IHttpActionResult> Get(int LearningCentersId, int CourseId)
        {
            var CoursesList = LCRepo.FindByID(LearningCentersId).Courses;
            var ReviewsList = CourseRepo.FindByID(CourseId).Reviews;
            if (ReviewsList == null || ReviewsList.Count() == 0)
            {
                return Content(HttpStatusCode.NotFound, "Reviews Under Particular Course not found");

            }
            else

                //   Mapper.CreateMap(Course,Coursevw)

                return Content(HttpStatusCode.OK, ReviewsList);

        }

        // GET: api/LearningCenters/1/Course/1/Review/1
        [Route("LearningCenters/{LearningCentersId}/Course/{CourseId}/Review/{id}")]
        public async Task<IHttpActionResult> Get(int LearningCentersId, int CourseId, int id)
        {

            var HostingLearningCenter = LCRepo.FindByID(LearningCentersId);

            if (HostingLearningCenter == null)
            {
                return Content(HttpStatusCode.NotFound, "The Learning Center you are searching for does not exist");
            }

            var Course = HostingLearningCenter.Courses.Where(c => c.ID == id)?.First();

            if (Course == null)
            {
                return Content(HttpStatusCode.NotFound, "The Course for a particular Learning Center does not exist");
            }
            var Review = Course.Reviews.Where(i => i.ID == id)?.First();
            if (Review == null)
            {
                return Content(HttpStatusCode.NotFound, "The Review for a particular Course does not exist");
            }
            else
            {
                return Content(HttpStatusCode.OK, Review);
            }
        }

        // DELETE: api/LearningCenters/1/Course/1/Review/1
        [Route("LearningCenters/{LearningCentersId}/Course/{CourseId}/Review/{id}")]
        public async Task<IHttpActionResult> Delete(int LearningCentersId, int CourseId, int id)
        {

            var HostingLearningCenter = LCRepo.FindByID(LearningCentersId);

            if (HostingLearningCenter == null)
            {
                return Content(HttpStatusCode.NotFound, "The Learning Center you are searching for does not exist");
            }

            var Course = HostingLearningCenter.Courses.Where(c => c.ID == id)?.First();

            if (Course == null)
                return Content(HttpStatusCode.NotFound, "The Course for a particular Learning Center does not exist");

            var ReviewToDelete = Course.Reviews.Where(i => i.ID == id)?.First();
            if (ReviewToDelete == null)
                return Content(HttpStatusCode.NotFound, "The Review for a particular Course does not exist");

            else

                Course.Reviews.Remove(ReviewToDelete);
                UnitOfWork.commit();
                return Content(HttpStatusCode.OK, "Reviews deleted Successfully");
        }

        // DELETE: api/LearningCenters/1/Course/1/Reviews
        [Route("LearningCenters/{LearningCentersId}/Course/{CourseId}/Reviews")]
        public async Task<IHttpActionResult> Delete(int LearningCentersId, int CourseId)
        {

            var HostingLearningCenter = LCRepo.FindByID(LearningCentersId);
            var Course = CourseRepo.FindByID(CourseId);
            var ReviewtoDelete = Course.Reviews;

            foreach (var i in ReviewtoDelete.ToList())
                Course.Reviews.Remove(i);

            UnitOfWork.commit();
            return Content(HttpStatusCode.OK, "Reviews deleted Successfully");

        }

        // POST: api/LearningCenters/1/Course/1/Review
        [Route("LearningCenters/{LearningCentersId}/Course/{CourseId}/Review")]
        public async Task<IHttpActionResult> Post(int LearningCentersId,int CourseId, Review r)
        {
            var HostingLearningCenter = LCRepo.FindByID(LearningCentersId);
            var Course = CourseRepo.FindByID(CourseId);
            Course.Reviews.Add(r);

            UnitOfWork.commit();
            return Content(HttpStatusCode.OK, "Reviews added Successfully");


        }

    }
}