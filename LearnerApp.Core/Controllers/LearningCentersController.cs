﻿using LearnerApp.Models;
using LearnerApp.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace LearnerApp.Core.Controllers
{
    public class LearningCentersController : ApiController
    {

        public SQLUnitOfWork UnitOfWork { get; set; }
        private IRepository<LearningCenter> LCRepo { get; set; }

        public LearningCentersController()
        {
            UnitOfWork = new SQLUnitOfWork();
            LCRepo = UnitOfWork.LearningCenter;
        }



        // GET: api/LearningCenters
        public async Task<IHttpActionResult> Get()
        {
            var lcList = LCRepo.GetAll().ToList();
            if (lcList == null || lcList.Count() == 0)
            {
                return Content(HttpStatusCode.NotFound, "Learing center not found");

            }
            else

                //   Mapper.CreateMap(Course,Coursevw)

                return Content(HttpStatusCode.OK, lcList);

        }

        // GET: api/LearningCenters/5
        public async Task<IHttpActionResult> Get(int id)
        {
            var lc = LCRepo.Get(i => i.ID == id).FirstOrDefault();
            if (lc == null)
                return Content(HttpStatusCode.NotFound, "The Learning Center you are searching for does not exist");

            else

                return Content(HttpStatusCode.OK, lc);
        }



        // POST: api/LearningCenters
        public async Task<IHttpActionResult> Post(LearningCenter c)
        {
            
                LCRepo.Add(c);
                UnitOfWork.commit();
              return Content(HttpStatusCode.OK,"Learning Center added successfully");


        }

        // PUT: api/LearningCenters/5
        public async Task<IHttpActionResult> Put(int id, LearningCenter newLc)
        {
            var oldLC = LCRepo.Get(i => i.ID == id).FirstOrDefault();

            if (oldLC == null)
                return Content(HttpStatusCode.NotFound, "The Learning Center you are searching for does not exist");

            else

            {
                    oldLC.Address = newLc.Address ?? oldLC.Address;
                    oldLC.Courses = newLc.Courses?? oldLC.Courses;
                    oldLC.ModifiedBy = newLc.ModifiedBy ?? "Web API";
                    oldLC.ModifiedDate = (newLc.ModifiedDate== DateTime.MinValue)?DateTime.Now: newLc.ModifiedDate;
                    oldLC.Name = newLc.Name ?? oldLC.Name;

                    LCRepo.Update(oldLC);
                    UnitOfWork.commit();
                    return Content(HttpStatusCode.OK, "LearningCenter Updated Sucessfully");
                
            }


        }

        // DELETE: api/LearningCenters/5
        public async Task<IHttpActionResult> Delete(int id)
        {
            var coursetodelete = LCRepo.Get(i => i.ID == id).FirstOrDefault();
            if (coursetodelete == null)
            {
                return Content(HttpStatusCode.NotFound, "The Learning Center you are searching for does not exist");
            }
            else
            {
                LCRepo.Delete(id);
                UnitOfWork.commit();
                return Content(HttpStatusCode.NotFound, "Learning Center deleted Successfully");
                

            }



        }
    }
}
