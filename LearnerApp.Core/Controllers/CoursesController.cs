﻿
using LearnerApp.Models;
using LearnerApp.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;


namespace LearnerApp.Controllers
{
   // [Authorize]
    public class CoursesController : ApiController
    {

        public SQLUnitOfWork UnitOfWork { get; set; } 
        private IRepository<Course> CourseRepo { get; set; }

        public CoursesController()
        {
            UnitOfWork = new SQLUnitOfWork();
            CourseRepo = UnitOfWork.Course;
        }
        
        
        // GET: api/Courses
        public async Task<IHttpActionResult> Get()
        {
            var CoursesList = CourseRepo.GetAll().ToList();
            if (CoursesList == null || CoursesList.Count() == 0)
            {
                return NotFound();
            
            }
            else
                
             //   Mapper.CreateMap(Course,Coursevw)

                return Content(HttpStatusCode.OK, CoursesList);
            
        }

        // GET: api/Courses/5
        public async Task<IHttpActionResult> Get(int id)
        {
            var course = CourseRepo.Get(i=> i.ID == id).FirstOrDefault();
            if (course == null)
                return Content(HttpStatusCode.NotFound, "The course you are searching for does not exist");

            else

                return Content(HttpStatusCode.OK, course);
        }

        // POST: api/Courses
        public async Task<IHttpActionResult> Post(Course c)
        {
              CourseRepo.Add(c);
                UnitOfWork.commit();
                return Ok();
           

        }

        // PUT: api/Courses/5
        public async Task<IHttpActionResult> Put(int id, Course c)
        {
            var oldCourse = CourseRepo.Get(i => i.ID == id).FirstOrDefault();

            if (oldCourse == null)
                  return  NotFound();
            else

            {
                
                    c.ID = id;
                    CourseRepo.Update(oldCourse);
                    UnitOfWork.commit();
                    return Ok("Course Updated Sucessfully");
                
            }


        }

        // DELETE: api/Courses/5
        public async Task<IHttpActionResult> Delete(int id)
        {
            var coursetodelete = CourseRepo.Get(i => i.ID == id).FirstOrDefault();
            if (coursetodelete == null)
            {
                return Content(HttpStatusCode.NotFound, "Course Not found");
            }
            else
            {
                CourseRepo.Delete(id);
                UnitOfWork.commit();
                return Content(HttpStatusCode.NotFound, "Course deleted Successfully");

            }


            
        }
       
    }
}
