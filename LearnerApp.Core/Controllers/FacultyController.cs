﻿using LearnerApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using LearnerApp.Repositories;


namespace LearnerApp.Controllers
{
    
    
    public class FacultyController : ApiController
    {
        public SQLUnitOfWork UnitOfWork { get; set; }
        public IRepository<Faculty> FacultyRepo { get; set; }


        public FacultyController()
        {
            UnitOfWork = new SQLUnitOfWork();
            FacultyRepo = UnitOfWork.Faculty;
        }



        // GET: api/Faculty
        public IHttpActionResult Get()
        {

            var facultyList = FacultyRepo.GetAll().ToList();
            if (facultyList == null || facultyList.Count() == 0)
            {
                return NotFound();
            }

            else


                return Content(HttpStatusCode.OK, facultyList);
        }

          



              // GET: api/Faculty/5
        public IHttpActionResult Get(int id)
        {
            var faculty = FacultyRepo.Get(i => i.ID == id).FirstOrDefault();
            if (faculty == null)
            {
                return Content(HttpStatusCode.NotFound, "The faculty You are searching for does not Exist");
            }
            else
            {
                return Content(HttpStatusCode.OK,faculty);
            }
        }

        // POST: api/Faculty
        public IHttpActionResult Post([FromBody]Faculty f)
        {
            
                FacultyRepo.Add(f);
                UnitOfWork.commit();
                return Ok();
            
        }

        // PUT: api/Faculty/5
        public IHttpActionResult Put(int id, [FromBody] Faculty f)
        {
            var oldfaculty = FacultyRepo.Get(i=> i.ID == id).FirstOrDefault();
            if (oldfaculty == null)
            {
                return NotFound();
            }
            else
            {
                
                    //  f.ID = id;

                    //todo Entity Copy
                    FacultyRepo.Update(oldfaculty);
                    UnitOfWork.commit();
                    return Ok("Faculty added Successfuly");
                
            }
          
        }

        // DELETE: api/Faculty/5
        public IHttpActionResult Delete(int id)
        {
            var facultyToRemove = FacultyRepo.Get(i => i.ID == id).FirstOrDefault();
            if (facultyToRemove == null)
            {
              
                return Ok("Faculty not found");
            }
            else
            {
               FacultyRepo.Delete(id);
                UnitOfWork.commit();
                return Ok("Faculty deleted successfully");
            }
        }

    }

}