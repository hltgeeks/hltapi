﻿using LearnerApp.Models;
using LearnerApp.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace LearnerApp.Core.Controllers
{
    public class DemosController : ApiController
    {
        public SQLUnitOfWork UnitOfWork { get; set; }
        private IRepository<Demo> DemoRepo { get; set; }

        public DemosController()
        {
            UnitOfWork = new SQLUnitOfWork();
            DemoRepo = UnitOfWork.Demo;
        }

        // GET: api/Demos
        public async Task<IHttpActionResult> Get()
        {
            var dList = DemoRepo.GetAll().ToList();
            if (dList == null || dList.Count() == 0)
            {
                return Content(HttpStatusCode.NotFound, "Demos not found");

            }
            else

                return Content(HttpStatusCode.OK, dList);

        }

        // GET: api/Demos/5
        public async Task<IHttpActionResult> Get(int id)
        {
            var lc = DemoRepo.Get(i => i.ID == id).FirstOrDefault();
            if (lc == null)
                return Content(HttpStatusCode.NotFound, "Demo you are searching for does not exist");

            else

                return Content(HttpStatusCode.OK, lc);
        }

        // POST: api/Demos
        public async Task<IHttpActionResult> Post(Demo d)
        {

            DemoRepo.Add(d);
            UnitOfWork.commit();
            return Content(HttpStatusCode.OK, "Demo added successfully");

        }

        // PUT: api/LearningCenters/5
        public async Task<IHttpActionResult> Put(int id, Demo newD)
        {
            var oldD = DemoRepo.Get(i => i.ID == id).FirstOrDefault();

            if (oldD == null)
                return Content(HttpStatusCode.NotFound, "Demo you are searching for does not exist");

            else

            {
               
                oldD.Course = newD.Course ?? oldD.Course;
                oldD.ModifiedBy = newD.ModifiedBy ?? "Shwe";
                oldD.ModifiedDate = (newD.ModifiedDate == DateTime.MinValue) ? DateTime.Now : newD.ModifiedDate;
                oldD.FromTime = (newD.FromTime == DateTime.MinValue) ? DateTime.Now : newD.FromTime;
                oldD.ToTime = (newD.ToTime == DateTime.MinValue) ? DateTime.Now : newD.ToTime;

                DemoRepo.Update(oldD);
                UnitOfWork.commit();
                return Content(HttpStatusCode.OK, "Demo Updated Sucessfully");

            }

        }

        // DELETE: api/Demos/5
        public async Task<IHttpActionResult> Delete(int id)
        {
            var coursetodelete = DemoRepo.Get(i => i.ID == id).FirstOrDefault();
            if (coursetodelete == null)
            {
                return Content(HttpStatusCode.NotFound, "Demo you are searching for does not exist");
            }
            else
            {
                DemoRepo.Delete(id);
                UnitOfWork.commit();
                return Content(HttpStatusCode.NotFound, "Demo deleted Successfully");

            }
        }
    }
}