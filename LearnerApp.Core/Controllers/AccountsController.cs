﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using LearnerApp.Models;
using LearnerApp.Repositories;

namespace LearnerApp.Controllers
{
    
    public class AccountsController : ApiController
    {
        
        public SQLUnitOfWork UnitOfWork { get; set; }
        private IRepository<Account> AccountsRepo { get; set; }

        private LearnersAppDB db = new LearnersAppDB();


        public AccountsController()
        {
            UnitOfWork = new SQLUnitOfWork();
            AccountsRepo = UnitOfWork.Account;
        }


        //// GET: api/Accounts
        //public IQueryable<Account> GetAccounts()
        //{
        //    return db.Accounts;
        //}

        //// GET: api/Accounts/5
        //[ResponseType(typeof(Account))]
        //public IHttpActionResult GetAccount(int id)
        //{
        //    Account account = db.Accounts.Find(id);
        //    if (account == null)
        //    {
        //        return NotFound();
        //    }

        //    return Ok(account);
        //}

        // PUT: api/Accounts/5
        [ResponseType(typeof(void))]
        public IHttpActionResult Put(int id, Account a)
        {
            var oldaccount = AccountsRepo.Get(i=>i.UserID == id).FirstOrDefault();
            if (oldaccount == null)
                   return NotFound();
            else
            {
                
                    AccountsRepo.Update(oldaccount);
                    UnitOfWork.commit();
                    return Ok("Account Updated Successfully");
                
            }

            
        }

        // POST: api/Accounts
       
        public IHttpActionResult Post(Account a)
        {
            
                AccountsRepo.Add(a);
                UnitOfWork.commit();
                return Ok();
            
        }

        // DELETE: api/Accounts/5
       
        public IHttpActionResult Delete(int id)
        {
            var EntityToBeDeleted = AccountsRepo.Get(i=>i.UserID ==id).FirstOrDefault();
            if (EntityToBeDeleted == null)
            {
                return Ok("Account Not Found");
            }
            else  
            {
                AccountsRepo.Delete(id);
                UnitOfWork.commit();
                return Ok("Account Deleted Successfully");
            }
        }

        //protected override void Dispose(bool disposing)
        //{
        //    if (disposing)
        //    {
        //        db.Dispose();
        //    }
        //    base.Dispose(disposing);
        //}

        //private bool AccountExists(int id)
        //{
        //    return db.Accounts.Count(e => e.UserID == id) > 0;
        //}
    }
}