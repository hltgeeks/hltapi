﻿using LearnerApp.Models;
using LearnerApp.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace LearnerApp.Core.Controllers
{
    [RoutePrefix("api")]
    public class CategoryLearningCentersController : ApiController
    {
        public SQLUnitOfWork UnitOfWork { get; set; }
        public IRepository<Category> CategoryRepo { get; set; }
        public IRepository<LearningCenter> LCRepo { get; set; }

        public CategoryLearningCentersController()
        {
            UnitOfWork = new SQLUnitOfWork();
            CategoryRepo = UnitOfWork.Category;
            LCRepo = UnitOfWork.LearningCenter;
        }

        // GET: api/Categories/1LearningCenters/1
        [Route("Categories/{CategoriesId}/LearningCenters/{id}")]

        public async Task<IHttpActionResult> Get(int CategoriesId, int id)
        {
            var Category = CategoryRepo.FindByID(CategoriesId);

            if (Category == null)
            {
                return Content(HttpStatusCode.NotFound, "The Category you are searching for does not exist");
            }
            var LearningCenter = CategoryRepo.FindByID(CategoriesId).LearningCenters.Where(i => i.ID == id);
            if (LearningCenter.Count() == 0)
                return Content(HttpStatusCode.NotFound, "The Learning Center for a particular Category does not exist");

            else

                return Content(HttpStatusCode.OK, LearningCenter.First());
        }

        // DELETE: api/Categories/1LearningCenters/1
        [Route("Categories/{CategoriesId}/LearningCenters/{id}")]
        public async Task<IHttpActionResult> Delete(int CategoriesId, int id)
        {
            var Category = CategoryRepo.FindByID(CategoriesId);

            if (Category == null)
            {
                return Content(HttpStatusCode.NotFound, "The Category you are searching for does not exist");
            }
            var LearningCenterToDelete = Category.LearningCenters.Where(i => i.ID == id)?.First();
            if (LearningCenterToDelete == null)
            {
                return Content(HttpStatusCode.NotFound, "The Learning Center for a particular Category does not exist");
            }
            else
            {
                Category.LearningCenters.Remove(LearningCenterToDelete);
                UnitOfWork.commit();
                return Content(HttpStatusCode.OK, "Learning Center deleted Successfully");

            }
        }

        // DELETE: api/Categories/1LearningCenters
        [Route("Categories/{CategoriesId}/LearningCenters")]
        public async Task<IHttpActionResult> Delete(int CategoriesId)
        {

            var Category = CategoryRepo.FindByID(CategoriesId);
            var LearningCentertoDelete = Category.LearningCenters;

            foreach (var i in LearningCentertoDelete.ToList())
                Category.LearningCenters.Remove(i);

            UnitOfWork.commit();
            return Content(HttpStatusCode.OK, "Learning Center deleted Successfully");

        }

        // POST: api/Categories/1LearningCenters
        [Route("Categories/{CategoriesId}/LearningCenters")]
        public async Task<IHttpActionResult> Post(int CategoriesId, LearningCenter l)
        {
            var Category = CategoryRepo.FindByID(CategoriesId);
            Category.LearningCenters.Add(l);

            UnitOfWork.commit();
            return Content(HttpStatusCode.OK, "Learning Center added Successfully");


        }
    }
}