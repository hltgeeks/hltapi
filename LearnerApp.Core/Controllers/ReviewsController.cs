﻿using LearnerApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using LearnerApp.Repositories;

namespace LearnerApp.Controllers
{
    public class ReviewsController : ApiController
    {
        public SQLUnitOfWork UnitOfWork { get; set; }
        public IRepository<Review> ReviewsRepo { get; set; }

        public ReviewsController()
        {
            UnitOfWork = new SQLUnitOfWork();
            ReviewsRepo = UnitOfWork.Review;
        }
        public ReviewsController(SQLUnitOfWork uof)
        {
            UnitOfWork = uof;
            ReviewsRepo = UnitOfWork.Review;
        }


        // GET: api/Reviews
        public IHttpActionResult Get()
        {
            var Reviewslist = ReviewsRepo.GetAll().ToList();
            if (Reviewslist == null || Reviewslist.Count() == 0)
            {
                return NotFound();
            }
            else
            {
                return Content(HttpStatusCode.OK, Reviewslist);
            }
        }

        // GET: api/Reviews/5
        public IHttpActionResult Get(int id)
        {
            var review = ReviewsRepo.Get(i => i.ID == id).FirstOrDefault();
            if (review == null)
            {
                return Content(HttpStatusCode.NotFound,"The Review you are searching for does not exist");
            }
            else
            {
                return Content(HttpStatusCode.OK, review);
            }
        }

        // POST: api/Reviews
        public IHttpActionResult Post(Review r)
        {
            
                ReviewsRepo.Add(r);
                UnitOfWork.commit();
                return Ok();
            
        }

        // PUT: api/Reviews/5
        public IHttpActionResult Put(int id,Review r)
        {
            var oldReview = ReviewsRepo.FindByID(id);
            if (oldReview == null)
            {
                return NotFound();
            }
            else
            {
                

                    oldReview.Title = r.Title;
                    oldReview.Summary = r.Summary;
                    oldReview.Ratings = r.Ratings;

                    ReviewsRepo.Update(oldReview);
                    UnitOfWork.commit();
                    return Ok("Review Updated Successfully");
               
            }
        }

        // DELETE: api/Reviews/5
        public IHttpActionResult Delete(int id)
        {
            var reviewtodelete = ReviewsRepo.Get(i => i.ID == id).FirstOrDefault();
            if (reviewtodelete == null)
            {
                return Ok("Review not Found");
            }
            else
            {
                ReviewsRepo.Delete(id);
                UnitOfWork.commit();
                return Ok("Review Deleted Successfully");
            }
                
            }
        }
    }

