﻿using LearnerApp.Models;
using LearnerApp.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace LearnerApp.Controllers
{
    [RoutePrefix("api")]
    public class LearningCenterReviewsController : ApiController
    {
        
        public SQLUnitOfWork UnitOfWork { get; set; }
        private IRepository<LearningCenter> LCRepo { get; set; }
        private IRepository<Review> ReviewsRepo { get; set; }

        public LearningCenterReviewsController()
        {
            UnitOfWork = new SQLUnitOfWork();
            LCRepo = UnitOfWork.LearningCenter;
            ReviewsRepo = UnitOfWork.Review;

        }

        // GET: api/LearningCenters/1/Reviews
        [Route("LearningCenters/{LearningCentersId}/Reviews")]
        public async Task<IHttpActionResult> Get(int LearningCentersId)
        {
            var ReviewsList = LCRepo.FindByID(LearningCentersId).Reviews;
            if (ReviewsList == null || ReviewsList.Count() == 0)
            {
                return Content(HttpStatusCode.NotFound, "Reviews not found");

            }
            else

                //   Mapper.CreateMap(Course,Coursevw)

                return Content(HttpStatusCode.OK, ReviewsList);

        }

        // GET: api/LearningCenters/1/Reviews/1
        [Route("LearningCenters/{LearningCentersId}/Reviews/{id}")]

        public async Task<IHttpActionResult> Get(int LearningCentersId, int id)
        {
            var HostingLearningCenter = LCRepo.FindByID(LearningCentersId);

            if (HostingLearningCenter == null)
            {
                return Content(HttpStatusCode.NotFound, "The Learning Center you are searching for does not exist");
            }
            var Review = LCRepo.FindByID(LearningCentersId).Reviews.Where(i => i.ID == id);
            if (Review.Count() == 0)
                return Content(HttpStatusCode.NotFound, "The Review for a particular Learning Center does not exist");

            else

                return Content(HttpStatusCode.OK, Review.First());
        }

        // DELETE: api/LearningCenters/1/Reviews/1
        [Route("LearningCenters/{LearningCentersId}/Reviews/{id}")]
        public async Task<IHttpActionResult> Delete(int LearningCentersId, int id)
        {
            var HostingLearningCenter = LCRepo.FindByID(LearningCentersId);

            if (HostingLearningCenter == null)
            {
                return Content(HttpStatusCode.NotFound, "The Learning Center you are searching for does not exist");
            }
            var ReviewToDelete = HostingLearningCenter.Reviews.Where(i => i.ID == id)?.First();
            if (ReviewToDelete == null)
            {
                return Content(HttpStatusCode.NotFound, "The Review for a particular Learning Center does not exist");
            }
            else
            {
                HostingLearningCenter.Reviews.Remove(ReviewToDelete);
                UnitOfWork.commit();
                return Content(HttpStatusCode.OK, "Reviews deleted Successfully");

            }
        }

        // DELETE: api/LearningCenters/1/Reviews
        [Route("LearningCenters/{LearningCentersId}/Reviews")]
        public async Task<IHttpActionResult> Delete(int LearningCentersId)
        {

            var HostingLearningCenter = LCRepo.FindByID(LearningCentersId);
            var ReviewtoDelete = HostingLearningCenter.Reviews;

            foreach (var i in ReviewtoDelete.ToList())
                HostingLearningCenter.Reviews.Remove(i);

            UnitOfWork.commit();
            return Content(HttpStatusCode.OK, "Reviews deleted Successfully");

        }

        // POST: api/LearningCenters/1/Reviews
        [Route("LearningCenters/{LearningCentersId}/Reviews")]
        public async Task<IHttpActionResult> Post(int LearningCentersId, Review r)
        {
            var HostingLearningCenter = LCRepo.FindByID(LearningCentersId);
            HostingLearningCenter.Reviews.Add(r);

            UnitOfWork.commit();
            return Content(HttpStatusCode.OK, "Reviews added Successfully");


        }

    }
}