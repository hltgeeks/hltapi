﻿using LearnerApp.Models;
using LearnerApp.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace LearnerApp.Core.Controllers
{
    public class CategoriesController : ApiController
    {
        public SQLUnitOfWork UnitOfWork { get; set; }
        private IRepository<Category> CategoryRepo { get; set; }

        public CategoriesController()
        {
            UnitOfWork = new SQLUnitOfWork();
            CategoryRepo = UnitOfWork.Category;

        }

        // GET: api/Categories
        public async Task<IHttpActionResult> Get()
        {
            var categoryList = CategoryRepo.GetAll().ToList();
            if (categoryList == null || categoryList.Count() == 0)
            {
                return Content(HttpStatusCode.NotFound, "Categories not found");

            }
            else

                //   Mapper.CreateMap(Course,Coursevw)

                return Content(HttpStatusCode.OK, categoryList);
        }

        // GET: api/Categories/5
        public async Task<IHttpActionResult> Get(int id)
        {
            var category = CategoryRepo.Get(i => i.ID == id).FirstOrDefault();
            if (category == null)
                return Content(HttpStatusCode.NotFound, "Categories you are searching for does not exist");

            else

                return Content(HttpStatusCode.OK, category);
        }

        // POST: api/Categories
        public async Task<IHttpActionResult> Post(Category c)
        {

            CategoryRepo.Add(c);
            UnitOfWork.commit();
            return Content(HttpStatusCode.OK, "Category added successfully");


        }

        // PUT: api/Categories/5
        public async Task<IHttpActionResult> Put(int id, Category newCategory)
        {
            var oldCategory = CategoryRepo.Get(i => i.ID == id).FirstOrDefault();

            if (oldCategory == null)
                return Content(HttpStatusCode.NotFound, "The Category you are searching for does not exist");

            else

            {
                oldCategory.Name = newCategory.Name ?? oldCategory.Name;
                
                CategoryRepo.Update(oldCategory);
                UnitOfWork.commit();
                return Content(HttpStatusCode.OK, "Category Updated Sucessfully");

            }


        }

        // DELETE: api/Categories/5
        public async Task<IHttpActionResult> Delete(int id)
        {
            var coursetodelete = CategoryRepo.Get(i => i.ID == id).FirstOrDefault();
            if (coursetodelete == null)
            {
                return Content(HttpStatusCode.NotFound, "Category you are searching for does not exist");
            }
            else
            {
                CategoryRepo.Delete(id);
                UnitOfWork.commit();
                return Content(HttpStatusCode.NotFound, "Category deleted Successfully");


            }



        }

    }
}
