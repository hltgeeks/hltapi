﻿using LearnerApp.Models;
using LearnerApp.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Collections;

namespace LearnerApp.Controllers
{
    [RoutePrefix("api")]
    public class LearningCenterCoursesController : ApiController
    {
        public SQLUnitOfWork UnitOfWork { get; set; }
        private IRepository<LearningCenter> LCRepo { get; set; }
        private IRepository<Course> CourseRepo { get; set; }


        public LearningCenterCoursesController()
        {
            UnitOfWork = new SQLUnitOfWork();
            LCRepo = UnitOfWork.LearningCenter;
            CourseRepo = UnitOfWork.Course;

        }
        // GET: api/LearningCenters/1/Courses
        [Route("LearningCenters/{LearningCentersId}/Courses")]
        public async Task<IHttpActionResult> Get(int LearningCentersId)
        {
            var CoursesList = LCRepo.FindByID(LearningCentersId).Courses;
            if (CoursesList == null || CoursesList.Count() == 0)
            {
                return Content(HttpStatusCode.NotFound, "Courses not found");

            }
            else

                //   Mapper.CreateMap(Course,Coursevw)

                return Content(HttpStatusCode.OK, CoursesList);

        }


        // GET: api/LearningCenters/1/Courses/1
        [Route("LearningCenters/{LearningCentersId}/Courses/{id}")]

        public async Task<IHttpActionResult> Get(int LearningCentersId, int id)
        {

            var HostingLearningCenter = LCRepo.FindByID(LearningCentersId);

            if (HostingLearningCenter == null)
            {
                return Content(HttpStatusCode.NotFound, "The Learning Center you are searching for does not exist");
            }

            var coursesToGet = HostingLearningCenter.Courses.Where(c => c.ID == id);

            if (coursesToGet.Count() == 0)
                return Content(HttpStatusCode.NotFound, "The Course for a particular Learning Center does not exist");

            else

                return Content(HttpStatusCode.OK, coursesToGet.First());
        }


        // DELETE: api/LearningCenters/1/Courses/1
        [Route("LearningCenters/{LearningCentersId}/Courses/{id}")]
        public async Task<IHttpActionResult> Delete(int LearningCentersId, int id)
        {
            var HostingLearningCenter = LCRepo.FindByID(LearningCentersId);


            //todo null check for LC
            if (HostingLearningCenter == null)
            {
                return Content(HttpStatusCode.NotFound, "The Learning Center you are searching for does not exist");
            }

            var coursesToDelete = HostingLearningCenter.Courses.Where(c => c.ID == id)?.First();
            if (coursesToDelete == null)
            {
                return Content(HttpStatusCode.NotFound, "The Course for a particular Learning Center you are searching for does not exist");
            }
            else
            {
                HostingLearningCenter.Courses.Remove(coursesToDelete);
                UnitOfWork.commit();
                return Content(HttpStatusCode.OK, "Courses deleted Successfully");

            }
        }

        // DELETE: api/LearningCenters/1/Courses
        [Route("LearningCenters/{LearningCentersId}/Courses")]
        public async Task<IHttpActionResult> Delete(int LearningCentersId)
        {

            var HostingLearningCenter = LCRepo.FindByID(LearningCentersId);
            var coursestoDelete = HostingLearningCenter.Courses;

            foreach (var c in coursestoDelete.ToList())
                HostingLearningCenter.Courses.Remove(c);

            UnitOfWork.commit();
            return Content(HttpStatusCode.OK, "Courses deleted Successfully");


        }

        // POST: api/LearningCenters/1/Courses
        [Route("LearningCenters/{LearningCentersId}/Courses")]
        public async Task<IHttpActionResult> Post(int LearningCentersId, Course c)
        {

            var HostingLearningCenter = LCRepo.FindByID(LearningCentersId);
            HostingLearningCenter.Courses.Add(c);

            UnitOfWork.commit();
            return Content(HttpStatusCode.OK, "Courses Added Successfully");

        }

    }
}