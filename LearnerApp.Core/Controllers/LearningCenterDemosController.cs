﻿using LearnerApp.Models;
using LearnerApp.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace LearnerApp.Core.Controllers
{
    [RoutePrefix("api")]
    public class LearningCenterDemosController : ApiController
    {
        public SQLUnitOfWork UnitOfWork { get; set; }
        private IRepository<LearningCenter> LCRepo { get; set; }
        private IRepository<Demo> DemoRepo { get; set; }

        public LearningCenterDemosController()
        {
            UnitOfWork = new SQLUnitOfWork();
            LCRepo = UnitOfWork.LearningCenter;
            DemoRepo = UnitOfWork.Demo;

        }

        // GET: api/LearningCenters/1/Demos
        [Route("LearningCenters/{LearningCentersId}/Demos")]
        public async Task<IHttpActionResult> Get(int LearningCentersId)
        {
            var DemosList = LCRepo.FindByID(LearningCentersId).Demos;
            if (DemosList == null || DemosList.Count() == 0)
            {
                return Content(HttpStatusCode.NotFound, "Demos not found");

            }
            else
                
                return Content(HttpStatusCode.OK, DemosList);

        }

        // GET: api/LearningCenters/1/Demos/1
        [Route("LearningCenters/{LearningCentersId}/Demo/{id}")]

        public async Task<IHttpActionResult> Get(int LearningCentersId, int id)
        {
            var HostingLearningCenter = LCRepo.FindByID(LearningCentersId);

            if (HostingLearningCenter == null)
            {
                return Content(HttpStatusCode.NotFound, "The Learning Center you are searching for does not exist");
            }
            var Demo = LCRepo.FindByID(LearningCentersId).Demos.Where(i => i.ID == id);
            if (Demo.Count() == 0)
                return Content(HttpStatusCode.NotFound, "Demo for a particular Learning Center does not exist");

            else

                return Content(HttpStatusCode.OK, Demo.First());
        }

        // DELETE: api/LearningCenters/1/Demo/1
        [Route("LearningCenters/{LearningCentersId}/Demo/{id}")]
        public async Task<IHttpActionResult> Delete(int LearningCentersId, int id)
        {
            var HostingLearningCenter = LCRepo.FindByID(LearningCentersId);

            if (HostingLearningCenter == null)
            {
                return Content(HttpStatusCode.NotFound, "The Learning Center you are searching for does not exist");
            }
            var DemoToDelete = HostingLearningCenter.Demos.Where(i => i.ID == id);
            if (DemoToDelete.Count() == 0)
            {
                return Content(HttpStatusCode.NotFound, "Demo for a particular Learning Center does not exist");
            }
            else
            {
                HostingLearningCenter.Demos.Remove(DemoToDelete.First());
                UnitOfWork.commit();
                return Content(HttpStatusCode.OK, "Demo deleted Successfully");

            }
        }

        // DELETE: api/LearningCenters/1/Demos
        [Route("LearningCenters/{LearningCentersId}/Demos")]
        public async Task<IHttpActionResult> Delete(int LearningCentersId)
        {

            var HostingLearningCenter = LCRepo.FindByID(LearningCentersId);
            var DemotoDelete = HostingLearningCenter.Demos;

            foreach (var i in DemotoDelete.ToList())
                HostingLearningCenter.Demos.Remove(i);

            UnitOfWork.commit();
            return Content(HttpStatusCode.OK, "Demos deleted Successfully");

        }

        // POST: api/LearningCenters/1/Demo
        [Route("LearningCenters/{LearningCentersId}/Demo")]
        public async Task<IHttpActionResult> Post(int LearningCentersId, Demo d)
        {
            var HostingLearningCenter = LCRepo.FindByID(LearningCentersId);
            HostingLearningCenter.Demos.Add(d);

            UnitOfWork.commit();
            return Content(HttpStatusCode.OK, "Demos added Successfully");

        }
    }
}