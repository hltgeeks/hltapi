﻿using LearnerApp.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;

namespace LearnerApp.Repositories
{
    public class SQLRepository<T> : IRepository<T> where T : class
    {


        public LearnersAppDB SQLDBContext;
        public DbSet<T> dbset;

        public SQLRepository(LearnersAppDB dbContext)
        {
            SQLDBContext = dbContext;
            dbset = SQLDBContext.Set<T>();
        }

        public void Add(T t)
        {
            try
            {
                dbset.Add(t);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

         public bool Remove(T entityDelete)
        {
            try
            {
                if (SQLDBContext.Entry(entityDelete).State == EntityState.Detached)
                {
                    dbset.Attach(entityDelete);
                }
                dbset.Remove(entityDelete);
                return true;
            }
            catch (Exception)
            {

                return false;
            }
        }

        public void Delete(int id)
        {
            T entitytoDelete = dbset.Find(id);
            if (entitytoDelete != null)
            {
                Remove(entitytoDelete);
            }
        }



        public T FindByID(int id) {

            var entity = dbset.Find(id);

            return entity;

        }
        public IQueryable<T> Get(Expression<Func<T, bool>> predecate = null)
        {
            IQueryable<T> Results;
            try
            {
                Results = dbset;
                Results = Results.Where(predecate);

            }
            catch (Exception ex)
            {

                Console.Write(ex);
                throw;

            }
            return Results;

        }

      

        public bool Update(T entitytoUpdate)
        {
            try
            {

                dbset.Attach(entitytoUpdate);
                SQLDBContext.Entry(entitytoUpdate).State = EntityState.Modified;


                return true;
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex);
                
                return false;
            }
        }

     

        public IQueryable<T> GetAll()
        {
            return dbset;
        }

       
    }
}