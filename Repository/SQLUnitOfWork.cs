﻿using LearnerApp.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;


namespace LearnerApp.Repositories
{
    public class SQLUnitOfWork : IDisposable
    {
        
        public LearnersAppDB DatabaseContext;

        public SQLUnitOfWork()
        {
            DatabaseContext = new LearnersAppDB();
        }

        public SQLUnitOfWork(DbContext context)
        {
            DatabaseContext = (LearnersAppDB)context;
        }

        //Repo
        private IRepository<Category> _category;

        public IRepository<Category> Category
        {
            get
            {
                if (_category == null)
                {
                    _category = new SQLRepository<Category>(DatabaseContext);
                }
                return _category;
            }
            set { _category = value; }
        }

        private IRepository<Demo> _demo;
        public IRepository<Demo> Demo
        {
            get
            {
                if(_demo == null)
                {
                    _demo = new SQLRepository<Demo>(DatabaseContext);
                }
                return _demo;
            }
            set { _demo = value; }
        }


        private IRepository<LearningCenter> _learningCenter;

        public IRepository<LearningCenter> LearningCenter
        {
            get
            {
                if (_learningCenter == null)
                {
                    _learningCenter = new SQLRepository<LearningCenter>(DatabaseContext);

                }
                return _learningCenter;
            }
            set { _learningCenter = value; }
        }



        private IRepository<Admission> _admission;

        public IRepository<Admission> Admission
        {
            get
            {
                if (_admission==null)
                {
                    _admission = new SQLRepository<Admission>(DatabaseContext);
                    
                }
                return _admission;
            }
            set { _admission = value; }
        }


        private IRepository<Batch> _batch;
        public IRepository<Batch> Batch
        {
            get
            {
                if (_batch==null)
                {
                    _batch = new SQLRepository<Batch>(DatabaseContext);
                   
                }
                return _batch;

            }
            set { _batch = value; }
        }



        private IRepository<Course> _course;
        public IRepository<Course> Course
        {
            get
            {
                if (_course ==null)
                {
                    _course = new SQLRepository<Course>(DatabaseContext);

                }
                return _course;
            }
            set { _course = value; }
        }

        private IRepository<CourseType> _coursetype;
        public IRepository<CourseType> CourseType
        {
            get
            {
                if (_coursetype ==null)
                {
                    _coursetype = new SQLRepository<CourseType>(DatabaseContext);
                }
                return _coursetype;
            }
            set { _coursetype = value; }
        }


        private IRepository<Enquiry> _enquiry;
        public IRepository<Enquiry> Enquiry
        {
            get
            {
                if (_enquiry == null)
                {
                    _enquiry = new SQLRepository<Enquiry>(DatabaseContext);
                }
                return _enquiry;
            }
            set { _enquiry = value; }
        }



        private IRepository<Faculty> _faculty;
        public IRepository<Faculty> Faculty
        {
            get
            {
                if (_faculty==null)
                {
                    _faculty = new SQLRepository<Faculty>(DatabaseContext);
                }
                return _faculty;
            }
            set { _faculty = value; }
        }


        private IRepository<Review> _review;
        public IRepository<Review> Review
        {
            get
            {
                if (_review == null)
                {
                    _review = new SQLRepository<Review>(DatabaseContext);
                }
                return _review;
            }
            set { _review = value; }
        }



        private IRepository<Student> _student;
        public IRepository<Student> Student
        {
            get
            {
                if (_student==null)
                {
                    _student = new SQLRepository<Student>(DatabaseContext);
                }
                return _student;
            }
            set { _student = value; }
        }

        private IRepository<Account> _account;
        public IRepository<Account> Account
        {
            get
            {
                if(_account == null)
                {
                    _account = new SQLRepository<Account>(DatabaseContext);
                }
                return _account;
            }
            set
            {
                _account = value;
            }
        }

        private IRepository<Wallet> _wallet;
        public IRepository<Wallet> Wallet
        {
            get
            {
                if (_wallet == null)
                {
                    _wallet = new SQLRepository<Wallet>(DatabaseContext);
                   
                }
                return _wallet;
            }
            set { _wallet = value; }
        }

        public void commit()
        {
            DatabaseContext.SaveChanges();
        }

        public void Dispose()
        {
            Dispose();
            GC.SuppressFinalize(this);
        }
        public bool DeleteAllAndResetIdentity()
        {
            DatabaseContext.Database.ExecuteSqlCommand("DELETE  FROM Review");
            DatabaseContext.Database.ExecuteSqlCommand("DELETE  FROM Demoes");
            DatabaseContext.Database.ExecuteSqlCommand("DELETE  FROM Course");
            DatabaseContext.Database.ExecuteSqlCommand("DELETE  FROM LearningCenters");
            DatabaseContext.Database.ExecuteSqlCommand("DELETE  FROM Categories");           
            DatabaseContext.Database.ExecuteSqlCommand("DELETE  FROM Enquiry");          
            DatabaseContext.Database.ExecuteSqlCommand("DELETE  FROM CourseEnquiry");
            DatabaseContext.Database.ExecuteSqlCommand("DELETE  FROM Faculty");           
            DatabaseContext.Database.ExecuteSqlCommand("DELETE  FROM Student");

            DatabaseContext.Database.ExecuteSqlCommand("DBCC CHECKIDENT('Demoes', RESEED, 0)");
            DatabaseContext.Database.ExecuteSqlCommand("DBCC CHECKIDENT('Categories', RESEED, 0)");
            DatabaseContext.Database.ExecuteSqlCommand("DBCC CHECKIDENT('LearningCenters', RESEED, 0)");
            DatabaseContext.Database.ExecuteSqlCommand("DBCC CHECKIDENT('Course', RESEED, 0)");
            DatabaseContext.Database.ExecuteSqlCommand("DBCC CHECKIDENT('Enquiry', RESEED, 0)");
            DatabaseContext.Database.ExecuteSqlCommand("DBCC CHECKIDENT('Faculty', RESEED, 0)");
            DatabaseContext.Database.ExecuteSqlCommand("DBCC CHECKIDENT('Review', RESEED, 0)");
            DatabaseContext.Database.ExecuteSqlCommand("DBCC CHECKIDENT('Student', RESEED, 0)");
            return true;

        }
    }
}