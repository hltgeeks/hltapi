﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace LearnerApp.Repositories
{
   public interface IRepository<T> where T :class
    {
         void Add(T t);
         bool Update(T edit);
         void Delete(int id);
        IQueryable<T> Get(Expression<Func<T, bool>> predecate);
        IQueryable<T> GetAll();
        T FindByID(int id);


    }
}
