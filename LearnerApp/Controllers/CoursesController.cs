﻿
using LearnerApp.Models;
using LearnerApp.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace LearnerApp.Controllers
{
    public class CoursesController : ApiController
    {
       public SQLUnitOfWork UnitOfWork { get; set; } 
        private IRepository<Course> CourseRepo { get; set; }

        public CoursesController()
        {
            UnitOfWork = new SQLUnitOfWork();
            CourseRepo = UnitOfWork.Course;
        }
        
        
        // GET: api/Courses
        public IHttpActionResult Get()
        {
            var CoursesList = CourseRepo.GetAll().ToList();
            if (CoursesList == null || CoursesList.Count() == 0)
            {
                return NotFound();

            }
            else

                return Content(HttpStatusCode.OK, CoursesList);


        }

        // GET: api/Courses/5
        public IHttpActionResult Get(int id)
        {
            var course = CourseRepo.Get(i=> i.ID == id).FirstOrDefault();
            if (course == null)
                return Content(HttpStatusCode.NotFound, "The course you are searching for does not exist");

            else

                return Content(HttpStatusCode.OK, course);
        }

        // POST: api/Courses
        public IHttpActionResult Post(Course c)
        {
            if (ModelState.IsValid)
            {
                CourseRepo.Add(c);
                UnitOfWork.commit();
                return Ok();
            }
            else {
                return Content(HttpStatusCode.InternalServerError, "Error Adding Course");

            }

        }

        // PUT: api/Courses/5
        public IHttpActionResult Put(int id, Course c)
        {
            var oldCourse = CourseRepo.Get(i => i.ID == id).FirstOrDefault();

            if (oldCourse == null)
                  return  NotFound();
            else

            {
                if (ModelState.IsValid)
                {
                    c.ID = id;
                    //todo Entity Copy
                    CourseRepo.Update(oldCourse);
                    UnitOfWork.commit();
                    return Ok("Course Updated Sucessfully");
                }
                else {

                    return BadRequest("There are errors in the Form Entry");

                }
            }


        }

        // DELETE: api/Courses/5
        public IHttpActionResult Delete(int id)
        {
            var coursetodelete = CourseRepo.Get(i => i.ID == id).FirstOrDefault();
            if (coursetodelete == null)
            {
                return Ok("Course Not found");
            }
            else
            {
                CourseRepo.Delete(id);
                UnitOfWork.commit();
                return Ok("Course deleted Successfully");

            }


            
        }
       
    }
}
