﻿using LearnerApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using LearnerApp.Repositories;


namespace LearnerApp.Controllers
{
    public class StudentsController : ApiController
    {
        public SQLUnitOfWork UnitOfWork { get; set; }
       public IRepository<Student> StudentRepo { get; set; }


        public StudentsController()
        {
            UnitOfWork = new SQLUnitOfWork();
            StudentRepo = UnitOfWork.Student;
        }



        // GET: api/Students
        public IHttpActionResult Get()
        {
            try
            {
                var students = StudentRepo.GetAll().ToList();
                if (students == null || students.Count() == 0)
                    return Content(HttpStatusCode.NoContent, "No Students added Yet");
                else
                    return Content(HttpStatusCode.OK, students);
            }
            catch (Exception ex) {

                return Content(HttpStatusCode.InternalServerError, "There is an Error inside the Server" + ex.Message);
            }

        }

        // GET: api/Students/5
        public IHttpActionResult Get(int id)
        {
            try
            {
                var student = StudentRepo.Get(i => i.StudentId == id).FirstOrDefault();
                if (student == null )
                    return Content(HttpStatusCode.NoContent, "No Students added Yet");
                else
                    return Content(HttpStatusCode.OK, student);
            }
            catch (Exception ex)
            {

                return Content(HttpStatusCode.InternalServerError, "There is an Error inside the Server" + ex.Message);
            }

        }

        // POST: api/Students
        public IHttpActionResult Post([FromBody]Student s)
        {

            try
            {
                if (ModelState.IsValid)
                {
                    StudentRepo.Add(s);
                    UnitOfWork.commit();
                    return Content(HttpStatusCode.OK, "Student Added sucessfully");
                }
                else
                {
                    return Content(HttpStatusCode.InternalServerError, "Error Adding Student");
                }
            }
            catch (Exception ex)
            {

                return Content(HttpStatusCode.InternalServerError, "There is an Error inside the Server" + ex.Message);
            }




        }

        // PUT: api/Students/5
        public IHttpActionResult Put(int id, [FromBody]Student s)
        {

            //try
            //{ 

            //    s.StudentId = id;
            //    StudentRepo.Update(s);
            //    UnitOfWork.commit();
            //    return Content(HttpStatusCode.OK, "Student Updated sucessfully");
            //}
            //catch (Exception ex)
            //{

            //    return Content(HttpStatusCode.InternalServerError, "There is an Error inside the Server" + ex.Message);
            //}
            return null;
        }

        // DELETE: api/Students/5
        public IHttpActionResult Delete(int id)
        {

            try
            {

                var studentstoDelete = StudentRepo.Get(i => i.StudentId == id).FirstOrDefault();
                if (studentstoDelete == null)
                {
                    return Ok("Student Not found");
                }
                else
                {
                    StudentRepo.Delete(id);
                    UnitOfWork.commit();
                    return Ok("Student deleted Successfully");

                }

            }
            catch (Exception ex)
            {

                return Content(HttpStatusCode.InternalServerError, "There is an Error inside the Server" + ex.Message);
            }

        }
    }
}
