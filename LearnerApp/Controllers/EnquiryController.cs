﻿using LearnerApp.Repositories;
using System.Linq;
using System.Net;
using System.Web.Http;
using LearnerApp.Models;
using System;
using System.Collections.Generic;
using System.Net.Http;

namespace LearnerApp.Controllers
{
    public class EnquiryController : ApiController
    {
   

        public SQLUnitOfWork UnitOfWork { get; set; }
        private IRepository<Enquiry> EnquiryRepo { get; set; }

        public EnquiryController()
        {
         
        }

        public EnquiryController(SQLUnitOfWork uof)
        {
            UnitOfWork = uof;
            EnquiryRepo = UnitOfWork.Enquiry;
        }



        // GET: api/Enquiry
        public IHttpActionResult Get()
        {
            var EnquiryList = EnquiryRepo.GetAll().ToList();
            if (EnquiryList == null || EnquiryList.Count() == 0)
            {
                return NotFound();
            }
            else
            {
                return Content(HttpStatusCode.OK, EnquiryList);
            }
        }

        // GET: api/Enquiry/5
        public IHttpActionResult Get(int id)
        {
            var enquiry = EnquiryRepo.Get(i=>i.ID==id).FirstOrDefault();
            if (enquiry == null)
            {
                return Content(HttpStatusCode.NotFound, "The Enquiry you are searching for does not exist");
            }
            else
            {
                return Content(HttpStatusCode.OK,enquiry);
            }
        }

        // POST: api/Enquiry
        public IHttpActionResult Post(Enquiry e)
        {
            if (ModelState.IsValid)
            {
                EnquiryRepo.Add(e);
                UnitOfWork.commit();
                return Ok();
            }
            else
            {
                return Content(HttpStatusCode.InternalServerError,"Error adding Enquiry");
            }
        }

        // PUT: api/Enquiry/5
        public IHttpActionResult Put(int id, Enquiry e)
        {
            var oldenquiry = EnquiryRepo.Get(i=>i.ID==id).FirstOrDefault();
            if (oldenquiry == null)
            {
                return NotFound();
            }
            else
            {
                if (ModelState.IsValid)
                {
                    e.ID = id;
                    //todo ENtity Copy
                    EnquiryRepo.Update(oldenquiry);
                    UnitOfWork.commit();
                    return Ok("Enquiry Updated Successfully");
                }
                else
                {
                    return BadRequest("There are Errors in the Form Entry");
                }
                   
            }
        }

        // DELETE: api/Enquiry/5
        public IHttpActionResult Delete(int id)
        {
            var enquirytodelete = EnquiryRepo.Get(i => i.ID == id).FirstOrDefault();
            if (enquirytodelete == null)
            {
                return Ok("Enquiry not Found");
            }
            else
            {
                EnquiryRepo.Delete(id);
                UnitOfWork.commit();
                return Ok("Enquiry Deleted Successfully");
            }
        }
    }
}
