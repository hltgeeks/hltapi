namespace Entity.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class BaseModelExperiment : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.LearningCenters", "CreateDate", c => c.DateTime(nullable: false, precision: 7, storeType: "datetime2"));
            AddColumn("dbo.LearningCenters", "ModifiedDate", c => c.DateTime(nullable: false, precision: 7, storeType: "datetime2"));
            AddColumn("dbo.LearningCenters", "CreatedBy", c => c.String());
            AddColumn("dbo.LearningCenters", "ModifiedBy", c => c.String());
            AddColumn("dbo.Branches", "CreateDate", c => c.DateTime(nullable: false, precision: 7, storeType: "datetime2"));
            AddColumn("dbo.Branches", "ModifiedDate", c => c.DateTime(nullable: false, precision: 7, storeType: "datetime2"));
            AddColumn("dbo.Branches", "CreatedBy", c => c.String());
            AddColumn("dbo.Branches", "ModifiedBy", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Branches", "ModifiedBy");
            DropColumn("dbo.Branches", "CreatedBy");
            DropColumn("dbo.Branches", "ModifiedDate");
            DropColumn("dbo.Branches", "CreateDate");
            DropColumn("dbo.LearningCenters", "ModifiedBy");
            DropColumn("dbo.LearningCenters", "CreatedBy");
            DropColumn("dbo.LearningCenters", "ModifiedDate");
            DropColumn("dbo.LearningCenters", "CreateDate");
        }
    }
}
