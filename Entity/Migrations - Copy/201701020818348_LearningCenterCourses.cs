namespace Entity.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class LearningCenterCourses : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Course", "LearningCenter_ID", "dbo.LearningCenters");
            DropIndex("dbo.Course", new[] { "LearningCenter_ID" });
            CreateTable(
                "dbo.LearningCenterCourses",
                c => new
                    {
                        LearningCenterID = c.Int(nullable: false),
                        CourseID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.LearningCenterID, t.CourseID })
                .ForeignKey("dbo.LearningCenters", t => t.LearningCenterID, cascadeDelete: true)
                .ForeignKey("dbo.Course", t => t.CourseID, cascadeDelete: true)
                .Index(t => t.LearningCenterID)
                .Index(t => t.CourseID);
            
            DropColumn("dbo.Course", "LearningCenter_ID");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Course", "LearningCenter_ID", c => c.Int());
            DropForeignKey("dbo.LearningCenterCourses", "CourseID", "dbo.Course");
            DropForeignKey("dbo.LearningCenterCourses", "LearningCenterID", "dbo.LearningCenters");
            DropIndex("dbo.LearningCenterCourses", new[] { "CourseID" });
            DropIndex("dbo.LearningCenterCourses", new[] { "LearningCenterID" });
            DropTable("dbo.LearningCenterCourses");
            CreateIndex("dbo.Course", "LearningCenter_ID");
            AddForeignKey("dbo.Course", "LearningCenter_ID", "dbo.LearningCenters", "ID");
        }
    }
}
