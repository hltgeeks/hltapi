// <auto-generated />
namespace Entity.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class CategoriesModel : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(CategoriesModel));
        
        string IMigrationMetadata.Id
        {
            get { return "201701161354570_CategoriesModel"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
