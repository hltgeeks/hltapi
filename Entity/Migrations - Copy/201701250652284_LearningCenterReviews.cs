namespace Entity.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class LearningCenterReviews : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Review", "LearningCenter_ID", c => c.Int());
            CreateIndex("dbo.Review", "LearningCenter_ID");
            AddForeignKey("dbo.Review", "LearningCenter_ID", "dbo.LearningCenters", "ID");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Review", "LearningCenter_ID", "dbo.LearningCenters");
            DropIndex("dbo.Review", new[] { "LearningCenter_ID" });
            DropColumn("dbo.Review", "LearningCenter_ID");
        }
    }
}
