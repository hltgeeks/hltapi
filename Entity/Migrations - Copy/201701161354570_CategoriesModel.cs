namespace Entity.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CategoriesModel : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Categories",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Description = c.String(),
                        CreateDate = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        ModifiedDate = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        CreatedBy = c.String(),
                        ModifiedBy = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            AddColumn("dbo.LearningCenters", "Category_ID", c => c.Int());
            CreateIndex("dbo.LearningCenters", "Category_ID");
            AddForeignKey("dbo.LearningCenters", "Category_ID", "dbo.Categories", "ID");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.LearningCenters", "Category_ID", "dbo.Categories");
            DropIndex("dbo.LearningCenters", new[] { "Category_ID" });
            DropColumn("dbo.LearningCenters", "Category_ID");
            DropTable("dbo.Categories");
        }
    }
}
