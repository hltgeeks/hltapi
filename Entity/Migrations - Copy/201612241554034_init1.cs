namespace Entity.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class init1 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Admission", "AdmissionDate", c => c.DateTime(nullable: false, precision: 7, storeType: "datetime2"));
            AlterColumn("dbo.Admission", "NextDueDate", c => c.DateTime(nullable: false, precision: 7, storeType: "datetime2"));
            AlterColumn("dbo.Course", "CreateDate", c => c.DateTime(nullable: false, precision: 7, storeType: "datetime2"));
            AlterColumn("dbo.Course", "ModifiedDate", c => c.DateTime(nullable: false, precision: 7, storeType: "datetime2"));
            AlterColumn("dbo.Course", "Date", c => c.DateTime(precision: 7, storeType: "datetime2"));
            AlterColumn("dbo.Enquiry", "Date", c => c.DateTime(nullable: false, precision: 7, storeType: "datetime2"));
            AlterColumn("dbo.Enquiry", "ExpectedDateOfJoin", c => c.DateTime(nullable: false, precision: 7, storeType: "datetime2"));
            AlterColumn("dbo.Enquiry", "CreateDate", c => c.DateTime(nullable: false, precision: 7, storeType: "datetime2"));
            AlterColumn("dbo.Enquiry", "ModifiedDate", c => c.DateTime(nullable: false, precision: 7, storeType: "datetime2"));
            AlterColumn("dbo.Student", "StudentDOB", c => c.DateTime(nullable: false, precision: 7, storeType: "datetime2"));
            AlterColumn("dbo.Student", "CreateDate", c => c.DateTime(nullable: false, precision: 7, storeType: "datetime2"));
            AlterColumn("dbo.Student", "ModifiedDate", c => c.DateTime(nullable: false, precision: 7, storeType: "datetime2"));
            AlterColumn("dbo.Batches", "StartDate", c => c.DateTime(nullable: false, precision: 7, storeType: "datetime2"));
            AlterColumn("dbo.Batches", "EndDate", c => c.DateTime(nullable: false, precision: 7, storeType: "datetime2"));
            AlterColumn("dbo.Faculty", "DOJ", c => c.DateTime(nullable: false, precision: 7, storeType: "datetime2"));
            AlterColumn("dbo.Faculty", "DOB", c => c.DateTime(nullable: false, precision: 7, storeType: "datetime2"));
            AlterColumn("dbo.Faculty", "CreateDate", c => c.DateTime(nullable: false, precision: 7, storeType: "datetime2"));
            AlterColumn("dbo.Faculty", "ModifiedDate", c => c.DateTime(nullable: false, precision: 7, storeType: "datetime2"));
            AlterColumn("dbo.Invoices", "DueDate", c => c.DateTime(nullable: false, precision: 7, storeType: "datetime2"));
            AlterColumn("dbo.Invoices", "PaidDate", c => c.DateTime(nullable: false, precision: 7, storeType: "datetime2"));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Invoices", "PaidDate", c => c.DateTime(nullable: false));
            AlterColumn("dbo.Invoices", "DueDate", c => c.DateTime(nullable: false));
            AlterColumn("dbo.Faculty", "ModifiedDate", c => c.DateTime(nullable: false));
            AlterColumn("dbo.Faculty", "CreateDate", c => c.DateTime(nullable: false));
            AlterColumn("dbo.Faculty", "DOB", c => c.DateTime(nullable: false));
            AlterColumn("dbo.Faculty", "DOJ", c => c.DateTime(nullable: false));
            AlterColumn("dbo.Batches", "EndDate", c => c.DateTime(nullable: false));
            AlterColumn("dbo.Batches", "StartDate", c => c.DateTime(nullable: false));
            AlterColumn("dbo.Student", "ModifiedDate", c => c.DateTime(nullable: false));
            AlterColumn("dbo.Student", "CreateDate", c => c.DateTime(nullable: false));
            AlterColumn("dbo.Student", "StudentDOB", c => c.DateTime(nullable: false));
            AlterColumn("dbo.Enquiry", "ModifiedDate", c => c.DateTime(nullable: false));
            AlterColumn("dbo.Enquiry", "CreateDate", c => c.DateTime(nullable: false));
            AlterColumn("dbo.Enquiry", "ExpectedDateOfJoin", c => c.DateTime(nullable: false));
            AlterColumn("dbo.Enquiry", "Date", c => c.DateTime(nullable: false));
            AlterColumn("dbo.Course", "Date", c => c.DateTime());
            AlterColumn("dbo.Course", "ModifiedDate", c => c.DateTime(nullable: false));
            AlterColumn("dbo.Course", "CreateDate", c => c.DateTime(nullable: false));
            AlterColumn("dbo.Admission", "NextDueDate", c => c.DateTime(nullable: false));
            AlterColumn("dbo.Admission", "AdmissionDate", c => c.DateTime(nullable: false));
        }
    }
}
