namespace Entity.Migrations
{
    using LearnerApp.Models;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<LearnerApp.Models.LearnersAppDB>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(LearnerApp.Models.LearnersAppDB context)
        {
            //context.CourseTypes.AddOrUpdate(new CourseType()
            //{
            //    Name = "WeekEndBatch",
            //    Description = "Sunday and Saturday Classes only",
            //    ClassDays = { DayOfWeek.Sunday, DayOfWeek.Saturday },
            //    ID = 1
            //});
            //context.CourseTypes.AddOrUpdate(new CourseType()
            //{
            //    Name = "WeekDayBatch",
            //    Description = "Sunday and Saturday Classes only",
            //    ClassDays = { DayOfWeek.Monday, DayOfWeek.Tuesday,
            //                                DayOfWeek.Wednesday, DayOfWeek.Thursday, DayOfWeek.Friday}
            //    ,
            //    ID = 2
            //});



            var course = new Course { DurationInHrs = 45, Name = "Java", Overview = "sdw", Price = 2000.0, Reviews = null, Syllabus = null, Trainers = null };

            context.Courses.AddOrUpdate(course);

            var category = new Category { Name = "HLT", Description = "abcd", CreatedBy = "Shwe", };

            context.Categories.AddOrUpdate(category);

        }
    }
}
