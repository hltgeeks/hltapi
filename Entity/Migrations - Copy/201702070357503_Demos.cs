namespace Entity.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Demos : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Demoes", "LearningCenter_ID", c => c.Int());
            CreateIndex("dbo.Demoes", "LearningCenter_ID");
            AddForeignKey("dbo.Demoes", "LearningCenter_ID", "dbo.LearningCenters", "ID");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Demoes", "LearningCenter_ID", "dbo.LearningCenters");
            DropIndex("dbo.Demoes", new[] { "LearningCenter_ID" });
            DropColumn("dbo.Demoes", "LearningCenter_ID");
        }
    }
}
