namespace Entity.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Release1 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Accounts",
                c => new
                    {
                        UserID = c.Int(nullable: false, identity: true),
                        UserName = c.String(nullable: false),
                        Password = c.String(nullable: false),
                        RefferalCode = c.String(),
                        AccountType = c.Int(nullable: false),
                        PhoneNo = c.String(),
                        Email = c.String(),
                    })
                .PrimaryKey(t => t.UserID);
            
            CreateTable(
                "dbo.Addresses",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        AddressLine1 = c.String(),
                        AddressLine2 = c.String(),
                        City = c.String(),
                        State = c.String(),
                        Latitude = c.Int(nullable: false),
                        Longitude = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.Admission",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        AdmissionDate = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        StudentID = c.Int(nullable: false),
                        EducatonLevel = c.Int(nullable: false),
                        InvoiceTotal = c.Double(nullable: false),
                        AmountPending = c.Double(nullable: false),
                        NextDueDate = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Student", t => t.StudentID, cascadeDelete: true)
                .Index(t => t.StudentID);
            
            CreateTable(
                "dbo.Course",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false),
                        Overview = c.String(nullable: false),
                        Syllabus = c.String(),
                        Price = c.Double(nullable: false),
                        DurationInHrs = c.Double(nullable: false),
                        CreateDate = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        ModifiedDate = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        CreatedBy = c.String(),
                        ModifiedBy = c.String(),
                        Date = c.DateTime(precision: 7, storeType: "datetime2"),
                        Domain = c.String(),
                        Discriminator = c.String(nullable: false, maxLength: 128),
                        Type_ID = c.Int(),
                        Admission_ID = c.Int(),
                        Admission_ID1 = c.Int(),
                        Admission_ID2 = c.Int(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.ProjectTypes", t => t.Type_ID)
                .ForeignKey("dbo.Admission", t => t.Admission_ID)
                .ForeignKey("dbo.Admission", t => t.Admission_ID1)
                .ForeignKey("dbo.Admission", t => t.Admission_ID2)
                .Index(t => t.Type_ID)
                .Index(t => t.Admission_ID)
                .Index(t => t.Admission_ID1)
                .Index(t => t.Admission_ID2);
            
            CreateTable(
                "dbo.Enquiry",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Date = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        Duration = c.String(),
                        JoinPurpose = c.String(nullable: false),
                        HowKnownAboutHlt = c.String(nullable: false),
                        TimeRequested = c.String(nullable: false),
                        ContactNum = c.String(nullable: false),
                        EmailId = c.String(nullable: false),
                        ExpectedDateOfJoin = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        EnquiryHandledBy = c.String(nullable: false),
                        ReasonForJoining = c.String(),
                        EnquiredStudentID = c.Int(nullable: false),
                        CreateDate = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        ModifiedDate = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        CreatedBy = c.String(),
                        ModifiedBy = c.String(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Student", t => t.EnquiredStudentID, cascadeDelete: true)
                .Index(t => t.EnquiredStudentID);
            
            CreateTable(
                "dbo.Student",
                c => new
                    {
                        StudentId = c.Int(nullable: false, identity: true),
                        StudentName = c.String(nullable: false),
                        EducationLevel = c.String(nullable: false),
                        CollegeName = c.String(nullable: false),
                        contactNum = c.String(nullable: false),
                        StudentAddress = c.String(nullable: false),
                        StudentDOB = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        StudentAge = c.Double(nullable: false),
                        CreateDate = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        ModifiedDate = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        CreatedBy = c.String(),
                        ModifiedBy = c.String(),
                        Demo_ID = c.Int(),
                    })
                .PrimaryKey(t => t.StudentId)
                .ForeignKey("dbo.Demoes", t => t.Demo_ID)
                .Index(t => t.Demo_ID);
            
            CreateTable(
                "dbo.Batches",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        StartDate = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        EndDate = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        TimeFrom = c.String(),
                        TimeTO = c.String(),
                        Status = c.Int(nullable: false),
                        CreatedBy = c.String(),
                        HandledBy_ID = c.Int(),
                        ProjectSubject_ID = c.Int(),
                        Subject_ID = c.Int(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Faculty", t => t.HandledBy_ID)
                .ForeignKey("dbo.Course", t => t.ProjectSubject_ID)
                .ForeignKey("dbo.Course", t => t.Subject_ID)
                .Index(t => t.HandledBy_ID)
                .Index(t => t.ProjectSubject_ID)
                .Index(t => t.Subject_ID);
            
            CreateTable(
                "dbo.Faculty",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false),
                        DOJ = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        DOB = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        Experience = c.Double(nullable: false),
                        CreateDate = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        ModifiedDate = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        CreatedBy = c.String(),
                        ModifiedBy = c.String(),
                        Demo_ID = c.Int(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Demoes", t => t.Demo_ID)
                .Index(t => t.Demo_ID);
            
            CreateTable(
                "dbo.LearningCenters",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        CreateDate = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        ModifiedDate = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        CreatedBy = c.String(),
                        ModifiedBy = c.String(),
                        Address_ID = c.Int(),
                        Category_ID = c.Int(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Addresses", t => t.Address_ID)
                .ForeignKey("dbo.Categories", t => t.Category_ID)
                .Index(t => t.Address_ID)
                .Index(t => t.Category_ID);
            
            CreateTable(
                "dbo.Demoes",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        FromTime = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        ToTime = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        CreateDate = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        ModifiedDate = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        CreatedBy = c.String(),
                        ModifiedBy = c.String(),
                        Course_ID = c.Int(),
                        LearningCenter_ID = c.Int(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Course", t => t.Course_ID)
                .ForeignKey("dbo.LearningCenters", t => t.LearningCenter_ID)
                .Index(t => t.Course_ID)
                .Index(t => t.LearningCenter_ID);
            
            CreateTable(
                "dbo.Review",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Title = c.String(nullable: false),
                        Summary = c.String(nullable: false),
                        Ratings = c.Double(nullable: false),
                        CourseID = c.Int(nullable: false),
                        Demo_ID = c.Int(),
                        LearningCenter_ID = c.Int(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Course", t => t.CourseID, cascadeDelete: true)
                .ForeignKey("dbo.Demoes", t => t.Demo_ID)
                .ForeignKey("dbo.LearningCenters", t => t.LearningCenter_ID)
                .Index(t => t.CourseID)
                .Index(t => t.Demo_ID)
                .Index(t => t.LearningCenter_ID);
            
            CreateTable(
                "dbo.Branches",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        ShortName = c.String(),
                        LongName = c.String(),
                        CreateDate = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        ModifiedDate = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        CreatedBy = c.String(),
                        ModifiedBy = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.ProjectTypes",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Description = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.Invoices",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        AmountPaid = c.Double(nullable: false),
                        AmountPendingInCurrentInvoice = c.Double(nullable: false),
                        DueDate = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        PaidDate = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        PaymentMode = c.Int(nullable: false),
                        PaidBy = c.String(),
                        Status = c.Int(nullable: false),
                        Student_StudentId = c.Int(),
                        Admission_ID = c.Int(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Student", t => t.Student_StudentId)
                .ForeignKey("dbo.Admission", t => t.Admission_ID)
                .Index(t => t.Student_StudentId)
                .Index(t => t.Admission_ID);
            
            CreateTable(
                "dbo.Categories",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Description = c.String(),
                        CreateDate = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        ModifiedDate = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        CreatedBy = c.String(),
                        ModifiedBy = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.CourseType",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false),
                        Description = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.Wallets",
                c => new
                    {
                        WId = c.Int(nullable: false, identity: true),
                        Amount = c.Double(nullable: false),
                    })
                .PrimaryKey(t => t.WId);
            
            CreateTable(
                "dbo.LearningCenterCourses",
                c => new
                    {
                        LearningCenterID = c.Int(nullable: false),
                        CourseID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.LearningCenterID, t.CourseID })
                .ForeignKey("dbo.LearningCenters", t => t.LearningCenterID, cascadeDelete: true)
                .ForeignKey("dbo.Course", t => t.CourseID, cascadeDelete: true)
                .Index(t => t.LearningCenterID)
                .Index(t => t.CourseID);
            
            CreateTable(
                "dbo.BranchProjects",
                c => new
                    {
                        Projectid = c.Int(nullable: false),
                        Branchid = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.Projectid, t.Branchid })
                .ForeignKey("dbo.Course", t => t.Projectid, cascadeDelete: true)
                .ForeignKey("dbo.Branches", t => t.Branchid, cascadeDelete: true)
                .Index(t => t.Projectid)
                .Index(t => t.Branchid);
            
            CreateTable(
                "dbo.BatchStudents",
                c => new
                    {
                        BatchID = c.Int(nullable: false),
                        StudentID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.BatchID, t.StudentID })
                .ForeignKey("dbo.Batches", t => t.BatchID, cascadeDelete: true)
                .ForeignKey("dbo.Student", t => t.StudentID, cascadeDelete: true)
                .Index(t => t.BatchID)
                .Index(t => t.StudentID);
            
            CreateTable(
                "dbo.CourseEnquiry",
                c => new
                    {
                        CourseID = c.Int(nullable: false),
                        EnquiryID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.CourseID, t.EnquiryID })
                .ForeignKey("dbo.Course", t => t.CourseID, cascadeDelete: true)
                .ForeignKey("dbo.Enquiry", t => t.EnquiryID, cascadeDelete: true)
                .Index(t => t.CourseID)
                .Index(t => t.EnquiryID);
            
            CreateTable(
                "dbo.FacultyCourses",
                c => new
                    {
                        CourseID = c.Int(nullable: false),
                        FacultyID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.CourseID, t.FacultyID })
                .ForeignKey("dbo.Course", t => t.CourseID, cascadeDelete: true)
                .ForeignKey("dbo.Faculty", t => t.FacultyID, cascadeDelete: true)
                .Index(t => t.CourseID)
                .Index(t => t.FacultyID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.LearningCenters", "Category_ID", "dbo.Categories");
            DropForeignKey("dbo.Course", "Admission_ID2", "dbo.Admission");
            DropForeignKey("dbo.Invoices", "Admission_ID", "dbo.Admission");
            DropForeignKey("dbo.Invoices", "Student_StudentId", "dbo.Student");
            DropForeignKey("dbo.Course", "Admission_ID1", "dbo.Admission");
            DropForeignKey("dbo.Course", "Admission_ID", "dbo.Admission");
            DropForeignKey("dbo.FacultyCourses", "FacultyID", "dbo.Faculty");
            DropForeignKey("dbo.FacultyCourses", "CourseID", "dbo.Course");
            DropForeignKey("dbo.CourseEnquiry", "EnquiryID", "dbo.Enquiry");
            DropForeignKey("dbo.CourseEnquiry", "CourseID", "dbo.Course");
            DropForeignKey("dbo.Enquiry", "EnquiredStudentID", "dbo.Student");
            DropForeignKey("dbo.Batches", "Subject_ID", "dbo.Course");
            DropForeignKey("dbo.BatchStudents", "StudentID", "dbo.Student");
            DropForeignKey("dbo.BatchStudents", "BatchID", "dbo.Batches");
            DropForeignKey("dbo.Batches", "ProjectSubject_ID", "dbo.Course");
            DropForeignKey("dbo.Course", "Type_ID", "dbo.ProjectTypes");
            DropForeignKey("dbo.BranchProjects", "Branchid", "dbo.Branches");
            DropForeignKey("dbo.BranchProjects", "Projectid", "dbo.Course");
            DropForeignKey("dbo.Review", "LearningCenter_ID", "dbo.LearningCenters");
            DropForeignKey("dbo.Demoes", "LearningCenter_ID", "dbo.LearningCenters");
            DropForeignKey("dbo.Student", "Demo_ID", "dbo.Demoes");
            DropForeignKey("dbo.Review", "Demo_ID", "dbo.Demoes");
            DropForeignKey("dbo.Review", "CourseID", "dbo.Course");
            DropForeignKey("dbo.Faculty", "Demo_ID", "dbo.Demoes");
            DropForeignKey("dbo.Demoes", "Course_ID", "dbo.Course");
            DropForeignKey("dbo.LearningCenterCourses", "CourseID", "dbo.Course");
            DropForeignKey("dbo.LearningCenterCourses", "LearningCenterID", "dbo.LearningCenters");
            DropForeignKey("dbo.LearningCenters", "Address_ID", "dbo.Addresses");
            DropForeignKey("dbo.Batches", "HandledBy_ID", "dbo.Faculty");
            DropForeignKey("dbo.Admission", "StudentID", "dbo.Student");
            DropIndex("dbo.FacultyCourses", new[] { "FacultyID" });
            DropIndex("dbo.FacultyCourses", new[] { "CourseID" });
            DropIndex("dbo.CourseEnquiry", new[] { "EnquiryID" });
            DropIndex("dbo.CourseEnquiry", new[] { "CourseID" });
            DropIndex("dbo.BatchStudents", new[] { "StudentID" });
            DropIndex("dbo.BatchStudents", new[] { "BatchID" });
            DropIndex("dbo.BranchProjects", new[] { "Branchid" });
            DropIndex("dbo.BranchProjects", new[] { "Projectid" });
            DropIndex("dbo.LearningCenterCourses", new[] { "CourseID" });
            DropIndex("dbo.LearningCenterCourses", new[] { "LearningCenterID" });
            DropIndex("dbo.Invoices", new[] { "Admission_ID" });
            DropIndex("dbo.Invoices", new[] { "Student_StudentId" });
            DropIndex("dbo.Review", new[] { "LearningCenter_ID" });
            DropIndex("dbo.Review", new[] { "Demo_ID" });
            DropIndex("dbo.Review", new[] { "CourseID" });
            DropIndex("dbo.Demoes", new[] { "LearningCenter_ID" });
            DropIndex("dbo.Demoes", new[] { "Course_ID" });
            DropIndex("dbo.LearningCenters", new[] { "Category_ID" });
            DropIndex("dbo.LearningCenters", new[] { "Address_ID" });
            DropIndex("dbo.Faculty", new[] { "Demo_ID" });
            DropIndex("dbo.Batches", new[] { "Subject_ID" });
            DropIndex("dbo.Batches", new[] { "ProjectSubject_ID" });
            DropIndex("dbo.Batches", new[] { "HandledBy_ID" });
            DropIndex("dbo.Student", new[] { "Demo_ID" });
            DropIndex("dbo.Enquiry", new[] { "EnquiredStudentID" });
            DropIndex("dbo.Course", new[] { "Admission_ID2" });
            DropIndex("dbo.Course", new[] { "Admission_ID1" });
            DropIndex("dbo.Course", new[] { "Admission_ID" });
            DropIndex("dbo.Course", new[] { "Type_ID" });
            DropIndex("dbo.Admission", new[] { "StudentID" });
            DropTable("dbo.FacultyCourses");
            DropTable("dbo.CourseEnquiry");
            DropTable("dbo.BatchStudents");
            DropTable("dbo.BranchProjects");
            DropTable("dbo.LearningCenterCourses");
            DropTable("dbo.Wallets");
            DropTable("dbo.CourseType");
            DropTable("dbo.Categories");
            DropTable("dbo.Invoices");
            DropTable("dbo.ProjectTypes");
            DropTable("dbo.Branches");
            DropTable("dbo.Review");
            DropTable("dbo.Demoes");
            DropTable("dbo.LearningCenters");
            DropTable("dbo.Faculty");
            DropTable("dbo.Batches");
            DropTable("dbo.Student");
            DropTable("dbo.Enquiry");
            DropTable("dbo.Course");
            DropTable("dbo.Admission");
            DropTable("dbo.Addresses");
            DropTable("dbo.Accounts");
        }
    }
}
