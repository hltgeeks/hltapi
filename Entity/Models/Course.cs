﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using LearnerApp.Models;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace LearnerApp.Models
{
    [Table("Course")]
    public class Course
    {


        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }
        [Required(ErrorMessage = "Error:Name Cannot be Empty")]
        public String Name { get; set; }
        [Required(ErrorMessage = "Error:Overview Cannot be Empty")]
        public string  Overview { get; set; }

        public string Syllabus { get; set; }
        
        public double Price { get; set; }

        public virtual ICollection<Enquiry> Enquiries { get; set; }

        public virtual ICollection<Faculty> Trainers { get; set; }
        public virtual ICollection<Review> Reviews { get; set; }
        public double DurationInHrs { get; set; }

        public DateTime CreateDate { get; set; }
        public DateTime ModifiedDate { get; set; }

        public String CreatedBy { get; set; }

        public String ModifiedBy { get; set; }

        public virtual ICollection<LearningCenter> HostingLearningCenters { get; set; }
    }
}