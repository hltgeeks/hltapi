﻿
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace LearnerApp.Models
{


  
    [Table("Enquiry")]
    public class Enquiry
    {

        public Enquiry()
        {

        }



        public Enquiry(int id,DateTime date, ICollection<Course> courses,string duration,string joinpurpose,string howknowaboutHLT,string timerequested,string contactnum,string emailid,DateTime expecteddateofjoin,string enquiryhandeledby,string reasonforjoining)
        {
            ID = id;
            Date = date;
            Courses = courses;
            Duration = duration;
            JoinPurpose = joinpurpose;
            HowKnownAboutHlt = howknowaboutHLT;
            TimeRequested = timerequested;
            ContactNum = contactnum;
            EmailId = emailid;
            ExpectedDateOfJoin = expecteddateofjoin;
            EnquiryHandledBy = enquiryhandeledby;
            ReasonForJoining = reasonforjoining;
            
        }
        private DateTime _date;
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }
        [Required(ErrorMessage="please enter the date")]
        [Display(Name = "Date")]
        [DataType(DataType.Date)]
        public DateTime Date {
            get { return _date;}
            set {_date = value;}
        }
       
      //  [Required(ErrorMessage="Select atleast one option")]
        [Display(Name = "Course")]
        public virtual ICollection<Course> Courses { get; set; }
    //    [Required(ErrorMessage="please enter the duration of course")]
        [Display(Name = "Duration in Months")]
       
        //[Range(1,24)]
        public string Duration { get; set; }

        [Required(ErrorMessage="enter the purpose of join")]
        [Display(Name = "Join Purpose")]
        public string JoinPurpose { get; set; }

        [Required]
        [Display(Name = "How u Know About HLT")]
        public string HowKnownAboutHlt { get; set; }

        [Required(ErrorMessage="Enter your convenient time")]
        [Display(Name = "Time Requested")]
        [DataType(DataType.Time)]
        public string TimeRequested { get; set; }

        [Required]
        [Display(Name = "Contact Number")]
        [DataType(DataType.PhoneNumber)]
        public string ContactNum { get; set; }

        [Required]
        [Display(Name="Email ID")]
        [DataType(DataType.EmailAddress)]
        public string EmailId { get; set; }

        [Required]
        [DataType(DataType.Date)]
        [Display(Name = "Expected Date of Joining")]
        public DateTime ExpectedDateOfJoin { get; set; }

        [Required]
        [Display(Name = "Enquiry Handled By")]
        public string EnquiryHandledBy { get; set; }

        [Display(Name = "Reason")]
        public string ReasonForJoining { get; set; }

        [ForeignKey("EnquiredStudentID")]
        public Student EnquiredStudent { get; set; }

        [Display(Name = "Enquired Students ID")]
        public int EnquiredStudentID { get; set; }


        public DateTime CreateDate { get; set; }
        public DateTime ModifiedDate { get; set; }

        public String CreatedBy { get; set; }

        public String ModifiedBy { get; set; }



        //[Required]
        //[Display(Name = "Demo FeedBack")]
        //public string DemoFeedback { get; set; }
        //[Required]
        //[Display(Name = "Infrastructure")]
        //public string Infrastructure { get; set; }

        //[Required(ErrorMessage="Select one option")] 
        //[Display(Name="Presentation Given?")]
        //public string PresentationGiven { get; set; }
        //[Required(ErrorMessage="Select one option")]
        //[Display(Name="Demo Shown?")]
        //public string DemoShown { get; set; }
        //[Required(ErrorMessage="Select one option")]
        //[Display(Name="Student FeedBack")]
        //public string StudentFeedback { get; set; }
        //[Required(ErrorMessage="Need to mention any one of the course" )] 
        //[Display(Name="Course Recommended")]
        //public string CourseRecommended { get; set; }

        //[Required(ErrorMessage="Select any one option")]
        //[Display(Name="Like To Join?")]
        //public string LikeToJoin { get; set; }

        //  public Course course { get; set; }
        //    public virtual ICollection<FollowUp> followup { get; set; }

    }
}