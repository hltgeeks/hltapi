﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LearnerApp.Models
{
public    class Address
    {
        public int ID { get; set; }
        public String AddressLine1 { get; set; }

        public String AddressLine2 { get; set; }


        public String City { get; set; }


        public String State { get; set; }

        public int Latitude { get; set; }

        public int Longitude { get; set; }


    }
}
