﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LearnerApp.Models
{
   // [Table("Wallet")]
    public class Wallet
    {
        [Key]
        public int WId { get; set; }
        [Required(ErrorMessage = "Amount Required")]
        public double Amount { get; set; }

    }
}
