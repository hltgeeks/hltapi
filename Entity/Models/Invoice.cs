﻿using LearnerApp.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;


namespace LearnerApp.Models
{
    public class Invoice
    {
        public int ID { get; set; }

        public Student Student { get; set; }

        public double AmountPaid { get; set; }

        public double AmountPendingInCurrentInvoice { get; set; }

        public DateTime DueDate { get; set; }

        public DateTime PaidDate { get; set; }
        [Required]
        public ModeOfPayment PaymentMode { get; set; }
        
        public String PaidBy { get; set; }

        [Required]
        public InvoiceStatus Status { get; set; }

        
    }
}