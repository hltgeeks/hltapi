﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LearnerApp.Models;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace LearnerApp.Models
{
   public class Project:Course
    {
       
        public virtual ProjectType Type { get; set; }
        public virtual ICollection<Branch> ProjectBranches { get; set; }

        public DateTime Date { get; set; }

        public String Domain { get; set; }




    }
}
