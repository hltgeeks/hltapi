﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LearnerApp.Models
{
    class BatchProgress
    {

        public int ID { get; set; }
        public String TopicsCovered { get; set; }
        public String FromTime { get; set; }
        public int ToTime { get; set; }
        public int Date { get; set; }

        public int BatchId { get; set; }
        [ForeignKey("BatchId")]
        public Batch Batch { get; set; }

        public int StudentId { get; set; }

        [ForeignKey("StudentId")]
        public Student Student { get; set; }

        public String CreatedBy { get; set; }
    }
}
