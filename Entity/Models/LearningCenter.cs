﻿using Entity.Models;
using LearnerApp.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LearnerApp.Models
{
    public class LearningCenter:BaseModel
    {
        public string Name { get; set; }

        public virtual Address Address { get; set; }

        public virtual ICollection<Course> Courses { get; set; }

        public virtual ICollection<Review> Reviews { get; set; }

        public virtual ICollection<Demo> Demos { get; set; }
         
    }

   
}
