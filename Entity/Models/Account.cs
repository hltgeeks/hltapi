﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LearnerApp.Models;

namespace LearnerApp.Models
{
    public enum UserType
    {
        Customized,
        Facebook,
        GooglePlus,

    }
   // [Table("Account")]
   public class Account
    {
        [Key]
        public int UserID { get; set; }

        [Required(ErrorMessage ="UserName cannot be empty")]
        public string UserName { get; set; }

        [Required(ErrorMessage ="Password cannot be empty")]
        public string Password { get; set; }

        public string RefferalCode { get; set; }

        public UserType AccountType { get; set; }

        public string PhoneNo { get; set; }

        public string Email { get; set; }

    }
}

