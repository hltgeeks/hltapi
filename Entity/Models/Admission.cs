﻿using LearnerApp.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;


namespace LearnerApp.Models
{
    [Table("Admission")]
    public class Admission
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }
        public DateTime AdmissionDate { get; set; }

        [ForeignKey("StudentID")]
        public Student AdmittingStudent { get; set; }

        public int StudentID { get; set; }
        public virtual ICollection< Course> AdmittingCourses { get; set; }
        public int EducatonLevel { get; set; }

        public ICollection<Course> Courses { get; set; }

        public ICollection<Project> Projects { get; set; }


        public ICollection<Invoice> Invoices { get; set; }

        public double InvoiceTotal { get; set; }
        public Double AmountPending { get; set; }

        public DateTime NextDueDate { get; set; }

    }
}