﻿using Entity.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LearnerApp.Models
{
  public  class Branch:BaseModel
    {
        public int ID { get; set; }
        public string ShortName { get; set; }
        public String LongName { get; set; }
        public virtual ICollection<Project> Projects { get; set; }

    }
}
