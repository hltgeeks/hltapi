﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace LearnerApp.Models
{
    [Table("Student")]
    public class Student
    {


        public Student()
        {

        }
        public Student(int studentId, string studentName, string educationLevel, string collegeName, string contactNum, string studentAddress, DateTime studentDOB, double studentAge, ICollection<Admission> admissions, ICollection<Enquiry> enquiries)
        {
            StudentId = studentId;
            StudentName = studentName;
            EducationLevel = educationLevel;
            CollegeName = collegeName;
            this.contactNum = contactNum;
            StudentAddress = studentAddress;
            StudentDOB = studentDOB;
            StudentAge = studentAge;
            Admissions = admissions;
            Enquiries = enquiries;
        }

        [Key]
        public int StudentId { get; set; }

        [Required(ErrorMessage="Enter the name of the Student ")]
        [Display(Name = "Student Name")]
        public string StudentName { get; set; }

       

        [Required(ErrorMessage="Select fron the list")]
        public string EducationLevel { get; set; }

        [Required(ErrorMessage="Enter the College name")]
        [Display(Name = "College Name")]
        public string CollegeName { get; set; }

        [Required]
        [Display(Name = "Contact Number")]
        [DataType(DataType.PhoneNumber)]
        public string contactNum { get; set; }

        [Required(ErrorMessage="Enter the Address")]
        [Display(Name = "Student Address")]
        public string StudentAddress { get; set; }

        [Required(ErrorMessage="Enter your Date of birth")]
        [DataType(DataType.Date)]
        [Display(Name = "Date Of Birth")]
        public DateTime StudentDOB { get; set; }
        [Required(ErrorMessage="Enter your age")]
        [Display(Name = "Student Age")]
        public double StudentAge { get; set; }

        public virtual ICollection<Admission> Admissions { get; set; }
        public virtual ICollection<Enquiry> Enquiries { get; set; }
        public virtual ICollection<Batch> Batches { get; set; }


        public DateTime CreateDate { get; set; }
        public DateTime ModifiedDate { get; set; }

        public String CreatedBy { get; set; }

        public String ModifiedBy { get; set; }
    }
}