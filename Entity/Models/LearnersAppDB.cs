﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;


namespace LearnerApp.Models
{
    public class LearnersAppDB : DbContext
    {
        public LearnersAppDB() : base("name=DefaultConnection")
        {

        }

        public DbSet<Faculty> Faculties { get; set; }

        public DbSet<CourseType> CourseTypes { get; set; }
        public DbSet<Course> Courses { get; set; }
        public DbSet<Student> Students { get; set; }
        public DbSet<Admission> Admissions { get; set; }
        public DbSet<Enquiry> Enquiries { get; set; }
        public DbSet<Review> Reviews { get; set; }
       
        public DbSet<Wallet> Wallet { get; set; }

        public DbSet<LearningCenter> LearningCenters { get; set; }

        public DbSet<Address> Addresses { get; set; }

        public DbSet<Category> Categories { get; set; }

        public DbSet<Demo> Demos { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Course>().
              HasMany(c => c.Trainers).
              WithMany(p => p.HandledCourses).
              Map(
               m =>
               {
                   m.MapLeftKey("CourseID");
                   m.MapRightKey("FacultyID");
                   m.ToTable("FacultyCourses");
               });
            modelBuilder.Properties<DateTime>().Configure(c => c.HasColumnType("datetime2"));

            modelBuilder.Entity<Course>().
                          HasMany(c => c.Enquiries).
                          WithMany(e => e.Courses).
                          Map(
                           m =>
                           {
                               m.MapLeftKey("CourseID");
                               m.MapRightKey("EnquiryID");
                               m.ToTable("CourseEnquiry");
                           });


            modelBuilder.Entity<LearningCenter>().
                      HasMany(LC => LC.Courses).
                      WithMany(c => c.HostingLearningCenters).
                      Map(
                       m =>
                       {
                           m.MapLeftKey("LearningCenterID");
                           m.MapRightKey("CourseID");
                           m.ToTable("LearningCenterCourses");
                       });


            modelBuilder.Entity<Project>().
                                   HasMany(c => c.ProjectBranches).
                                   WithMany(e => e.Projects).
                                   Map(
                                    m =>
                                    {
                                        m.MapLeftKey("Projectid");
                                        m.MapRightKey("Branchid");
                                        m.ToTable("BranchProjects");
                                    });

            modelBuilder.Entity<Batch>().
                                   HasMany(b => b.StudentsInBatch).
                                   WithMany(s => s.Batches).
                                   Map(
                                    m =>
                                    {
                                        m.MapLeftKey("BatchID");
                                        m.MapRightKey("StudentID");
                                        m.ToTable("BatchStudents");
                                    });



        }

        public System.Data.Entity.DbSet<Account> Accounts { get; set; }

        public System.Data.Entity.DbSet<Batch> Batches { get; set; }
     //   public System.Data.Entity.DbSet<Project> Projects { get; set; }
        public System.Data.Entity.DbSet<ProjectType> ProjectTypes{ get; set; }

    }
}