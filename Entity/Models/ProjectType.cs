﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LearnerApp.Models
{
  public class ProjectType
    {
        public int ID { get; set; }
        public string Name { get; set; }

        public String Description { get; set; }

    }
}
