﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace LearnerApp.Models
{
    [Table("CourseType")]
    public class CourseType
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }

        [Required(ErrorMessage ="Error:Name Cannot be Empty")]
        public String Name { get; set; }
        public string Description { get; set; }
        public virtual ICollection<DayOfWeek> ClassDays { get; set; }
  
    }
}