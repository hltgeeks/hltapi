﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LearnerApp.Models;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
namespace LearnerApp.Models
{
    public class Batch
    {
        public int ID { get; set; }
        public string Name { get; set; }

        public Course Subject { get; set; }

        public Project ProjectSubject { get; set; }



        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }

        public String TimeFrom { get; set; }

        public string TimeTO { get; set; }

        public Faculty HandledBy { get; set; }

        public virtual ICollection<Student> StudentsInBatch { get; set; }

        [Required]
        public BatchStatus Status { get; set; }

        public string CreatedBy { get; set; }
        
    }
}