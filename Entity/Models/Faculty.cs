﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LearnerApp.Models
{
    [Table("Faculty")]
    public class Faculty
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }
        [Required(ErrorMessage = "Error:Name Cannot be Empty")]
        public string Name { get; set; }
        public DateTime DOJ { get; set; }
        public DateTime DOB { get; set; }
        [Required(ErrorMessage = "Error:Experience Cannot be Empty")]
        public double Experience { get; set; }
        public virtual ICollection<Course> HandledCourses { get; set; }

        public DateTime CreateDate { get; set; }
        public DateTime ModifiedDate { get; set; }

        public String CreatedBy { get; set; }

        public String ModifiedBy { get; set; }
    }
}