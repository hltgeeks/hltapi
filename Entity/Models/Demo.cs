﻿using Entity.Models;
using LearnerApp.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LearnerApp.Models
{
    public class Demo : BaseModel
    {
        public virtual Course Course { get; set; } 

        public DateTime FromTime { get; set; }

        public DateTime ToTime { get; set; }

        public virtual ICollection<Student> Students { get; set; }

        public virtual ICollection<Review> Reviews { get; set; }

        public virtual ICollection<Faculty> Faculties { get; set; }
    }
}
