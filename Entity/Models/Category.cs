﻿using Entity.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using System.Text;
using System.Threading.Tasks;

namespace LearnerApp.Models
{
    public class Category:BaseModel
    {
        public string Name { get; set; }
        public string Description { get; set; }

        public virtual ICollection<LearningCenter> LearningCenters { get; set; }
    }
}
