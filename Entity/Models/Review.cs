﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace LearnerApp.Models
{
    [Table("Review")]
    public class Review
    {

        public Review()
        {

        }

        public Review(int id,string title,string summary,double ratings)
        {
            ID = id;
            Title = title;
            Summary = summary;
            Ratings = ratings;
        }



        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }

        [Required(ErrorMessage = "Please Enter the Title")]
        [Display(Name = "Title")]
        public string Title { get; set; }

        [Required]
        [Display(Name = "Summary")]
        public string Summary { get; set; }

        [Required(ErrorMessage = "Please Rate Us")]
        [Display(Name= "Rate Us" )]
        private double  _ratings;

        public double Ratings
        {
            get { return _ratings; }
            set {

                if(value<=5)
                 _ratings = value;
            }
        }

        [Display(Name = "Enquired Students ID")]
        public int CourseID { get; set; }
        [ForeignKey("CourseID")]
        public virtual Course ReviewedCourse{get;set;}
    
    }
}